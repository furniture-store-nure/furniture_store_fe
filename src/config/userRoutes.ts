import { RoutesConfig } from "./routesConfig";

export const userRoutes = [
	{ label: "Кошик", href: RoutesConfig.Cart, isShowInHeader: false },
	{ label: "Мої замовлення", href: RoutesConfig.Orders, isShowInHeader: true },
	{ label: "", href: RoutesConfig.Settings, isShowInHeader: false },
	{ label: "", href: RoutesConfig.CreateOrder, isShowInHeader: false },
	{ label: "", href: RoutesConfig.Comparison, isShowInHeader: false },
];
