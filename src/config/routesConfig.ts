export const RoutesConfig = {
	Home: "/",
	Login: "/auth/sign-in",
	Registration: "/auth/sign-up",
	Orders: "/orders",
	CreateOrder: "/create-order",
	Products: "/products",
	Cart: "/cart",
	Settings: "/settings",
	Comparison: "/comparison",
};
