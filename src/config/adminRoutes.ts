import { RoutesConfig } from "./routesConfig";

export const adminRoutes = [
	{ label: "Замовлення", href: RoutesConfig.Orders, isShowInHeader: true },
	{
		label: "Меблевi вироби",
		href: RoutesConfig.Products,
		isShowInHeader: true,
	},
];
