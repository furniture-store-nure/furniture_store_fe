import { useState } from "react";
import { type GetServerSideProps, type GetServerSidePropsContext } from "next";
import { useRouter } from "next/router";
import { getServerSession } from "next-auth";

import type { NextPageWithLayout } from "../_app";
import { AppLayout } from "~/components/layouts";
import { checkAccess } from "~/utils/ssr";
import { authOptions } from "~/server/auth";
import { useGetProducts, useProductColumns } from "~/hooks/products";
import { useAppDispatch, useAppSelector } from "~/redux/hooks";
import { Icon, Table } from "~/components/UI";
import { selectShowModal, setOpenModal } from "~/redux/app";
import {
	CreateProductModal,
	EditProductModal,
	DeleteProductModal,
} from "~/widgets/products";

const LIMIT = 10;

const ProductsPage: NextPageWithLayout = () => {
	const router = useRouter();
	const dispatch = useAppDispatch();
	const showModal = useAppSelector(selectShowModal);

	const queryPage = router.query.page ? Number(router.query.page) : 1;

	const [page, setPage] = useState<number>(queryPage);
	const [selectedProductId, setSelectedProductId] = useState<string | null>(
		null,
	);

	const handlePageChange = (page: number) => {
		setPage(page);
		router.replace(`?page=${page}`);
	};

	const handleOpenCreateProductModal = () => {
		dispatch(setOpenModal("create_product"));
	};

	const handleOpenEditProductModal = (productId: string) => {
		setSelectedProductId(productId);
		dispatch(setOpenModal("edit_product"));
	};

	const handleOpenDeleteProductModal = (productId: string) => {
		setSelectedProductId(productId);
		dispatch(setOpenModal("delete_product"));
	};

	const { columns } = useProductColumns({
		handleOpenEditProductModal,
		handleOpenDeleteProductModal,
	});

	const { products, count, refetch } = useGetProducts({ page, limit: LIMIT });

	const handleRefetchProducts = () => {
		refetch();
	};

	return (
		<>
			<div className="flex flex-col">
				<div className="my-10 ml-[30px] flex justify-between">
					<h2 className="text-2xl font-semibold text-gray">Меблевi вироби</h2>
					<button
						className="flex items-center gap-x-[10px] text-sm font-semibold tracking-wider text-gray"
						onClick={handleOpenCreateProductModal}
					>
						<Icon icon="add_box" />
						Створити новий вирiб
					</button>
				</div>
				{products.length > 0 && (
					<Table
						withVerticalLines={false}
						columns={columns}
						rows={products}
						rowName="Меблевi вироби"
						totalCount={count}
						limit={LIMIT}
						onChangePage={handlePageChange}
					/>
				)}
			</div>
			{showModal === "create_product" && (
				<CreateProductModal handleRefetchProducts={handleRefetchProducts} />
			)}
			{selectedProductId && showModal === "edit_product" && (
				<EditProductModal
					productId={selectedProductId}
					handleRefetchProducts={handleRefetchProducts}
				/>
			)}
			{selectedProductId && showModal === "delete_product" && (
				<DeleteProductModal
					productId={selectedProductId}
					handleRefetchProducts={handleRefetchProducts}
				/>
			)}
		</>
	);
};

ProductsPage.getLayout = (page) => <AppLayout>{page}</AppLayout>;

export default ProductsPage;

export const getServerSideProps: GetServerSideProps = async (
	context: GetServerSidePropsContext,
) => {
	const session = await getServerSession(context.req, context.res, authOptions);
	const accessCheck = checkAccess(session, context);

	if (accessCheck) {
		return accessCheck;
	}

	return {
		props: {},
	};
};
