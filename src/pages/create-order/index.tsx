import { useMemo } from "react";
import { type GetServerSideProps, type GetServerSidePropsContext } from "next";
import { getServerSession } from "next-auth";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { z } from "zod";
import { POSTS } from "@prisma/client";
import { useRouter } from "next/router";

import type { NextPageWithLayout } from "../_app";
import { AppLayout } from "~/components/layouts";
import { authOptions } from "~/server/auth";
import { checkAccess } from "~/utils/ssr";
import { DeliveryType, POST_OPTIONS } from "~/utils/constants";
import { api } from "~/utils/api";
import { useAuth, useMessage } from "~/hooks";
import { RoutesConfig } from "~/config/routesConfig";
import { selectUpdateRequest, setUpdateRequest } from "~/redux/updateRequest";
import { useAppDispatch, useAppSelector } from "~/redux/hooks";
import { type CreateOrderArgs } from "~/server/core/validations";
import {
	Button,
	ControlledInput,
	ControlledRadioButton,
	ControlledSelect,
	Form,
	Icon,
	Label,
} from "~/components/UI";

const selectOptionPostNameSchema = z.object({
	value: z.nativeEnum(POSTS),
	label: z.string(),
});

const selectOptionPostOfficeNumberSchema = z.object({
	value: z.string(),
	label: z.string(),
});

const formSchema = z.object({
	postName: selectOptionPostNameSchema,
	deliveryType: z.nativeEnum(DeliveryType),
	postOfficeNumber: selectOptionPostOfficeNumberSchema.optional(),
	addressDelivery: z.string().min(10, "Введіть адресу доставки").optional(),
});

type FormValues = z.input<typeof formSchema>;

const CreateOrderPage: NextPageWithLayout = () => {
	const dispatch = useAppDispatch();
	const router = useRouter();
	const m = useMessage();
	const { userId } = useAuth();
	const needsUpdate = useAppSelector(selectUpdateRequest);

	const form = useForm<FormValues>({
		mode: "onChange",
		resolver: zodResolver(formSchema),
	});

	const { errors } = form.formState;

	const { mutate: handleCreateOrder } = api.order.createOrder.useMutation({
		onSuccess: () => {
			m.success("Замовлення створено");
			dispatch(
				setUpdateRequest({ needsUpdate: [...needsUpdate, "orders", "countCarts"] }),
			);
			router.push(RoutesConfig.Home);
		},
		onError: (error) => {
			m.error(error.message);
		},
	});

	const numbersOfPostOffice = useMemo(() => {
		const numbers = [];
		for (let i = 1; i <= 100; i++) {
			numbers.push({ value: i.toString(), label: i.toString() });
		}
		return numbers;
	}, []);

	const handleSubmit = (data: FormValues) => {
		const order: CreateOrderArgs = {
			postName: data.postName.value,
			deliveryType: data.deliveryType,
			addressDelivery: undefined,
			postOfficeNumber: undefined,
			userId,
		};

		if (data.deliveryType === DeliveryType.PostOfficeNumberCheck) {
			order.postOfficeNumber = data.postOfficeNumber?.value;
		} else {
			order.addressDelivery = data.addressDelivery;
		}

		handleCreateOrder(order);
	};

	const isDisabled = !form.formState.isValid;

	return (
		<div className="mt-[200px] flex items-center justify-center">
			<Form
				form={form}
				onSubmit={handleSubmit}
				className="w-[600px] rounded-2xl bg-white shadow-card"
			>
				<span className="flex items-center gap-x-2 py-5 pl-6">
					<Icon icon="check" />
					<h2 className="text-xl font-semibold text-gray">
						Оформлення замовлення
					</h2>
				</span>
				<div className="w-full border-b-4 border-grayMedium" />
				<div className="mt-9 flex flex-col gap-y-5 px-6">
					<div className="flex items-center justify-between">
						<Label htmlFor="postName">Пошта</Label>
						<ControlledSelect
							name="postName"
							placeholder="Виберiть пошту"
							options={POST_OPTIONS}
						/>
					</div>
					<fieldset
						id="deliveryType"
						className="flex items-center justify-end gap-x-3"
					>
						<ControlledRadioButton
							name="deliveryType"
							value="postOfficeNumberCheck"
							label="Відділення"
						/>
						<ControlledRadioButton
							name="deliveryType"
							value="addressDeliveryCheck"
							label="Адреса доставки"
						/>
					</fieldset>
					{form.watch("deliveryType") ===
						DeliveryType.PostOfficeNumberCheck && (
						<div className="flex items-center justify-between">
							<Label htmlFor="postOfficeNumber">Номер відділення</Label>
							<ControlledSelect
								name="postOfficeNumber"
								placeholder="Введіть номер відділення"
								options={numbersOfPostOffice}
							/>
						</div>
					)}
					{form.watch("deliveryType") === DeliveryType.AddressDeliveryCheck && (
						<div className="flex items-center justify-between">
							<Label htmlFor="addressDelivery">Адреса доставки</Label>
							<ControlledInput
								className="w-[313px]"
								classNameError="w-[313px]"
								name="addressDelivery"
								placeholder="пр. Гагарiна 102"
								errorMessage={errors.addressDelivery?.message}
							/>
						</div>
					)}
				</div>
				<div className="mb-5 mt-9 flex justify-center">
					<Button disabled={isDisabled} type="submit">
						Замовити
					</Button>
				</div>
			</Form>
		</div>
	);
};

CreateOrderPage.getLayout = (page) => <AppLayout>{page}</AppLayout>;

export default CreateOrderPage;

export const getServerSideProps: GetServerSideProps = async (
	context: GetServerSidePropsContext,
) => {
	const session = await getServerSession(context.req, context.res, authOptions);
	const accessCheck = checkAccess(session, context);

	if (accessCheck) {
		return accessCheck;
	}

	return {
		props: {},
	};
};
