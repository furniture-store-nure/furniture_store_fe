import { type GetServerSideProps, type GetServerSidePropsContext } from "next";
import { getServerSession } from "next-auth";

import type { NextPageWithLayout } from "../../_app";
import { checkAccess } from "~/utils/ssr";
import { authOptions } from "~/server/auth";
import { AuthLayout } from "~/components/layouts";
import { SignIn } from "~/widgets/sign-in";

const SignInPage: NextPageWithLayout = () => {
	return (
		<>
			<SignIn />
		</>
	);
};

SignInPage.getLayout = (page) => <AuthLayout>{page}</AuthLayout>;

export default SignInPage;

export const getServerSideProps: GetServerSideProps = async (
	context: GetServerSidePropsContext,
) => {
	const session = await getServerSession(context.req, context.res, authOptions);
	const accessCheck = checkAccess(session, context);

	if (accessCheck) {
		return accessCheck;
	}

	return {
		props: {},
	};
};
