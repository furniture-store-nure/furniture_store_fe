import { useEffect } from "react";
import { type GetServerSideProps, type GetServerSidePropsContext } from "next";
import { useRouter } from "next/router";
import { getServerSession } from "next-auth";

import { api } from "~/utils/api";
import type { NextPageWithLayout } from "../_app";
import { AppLayout } from "~/components/layouts";
import { checkAccess } from "~/utils/ssr";
import { authOptions } from "~/server/auth";
import { useGetComparisons } from "~/hooks/comparison";
import { RoutesConfig } from "~/config/routesConfig";
import { ComparisonTable } from "~/widgets/comparison";
import { useMessage } from "~/hooks";

const ComparisonPage: NextPageWithLayout = () => {
	const m = useMessage();
	const router = useRouter();
	const { comparisons, isLoading, refetch } = useGetComparisons();

	const { mutate: DeleteComparisons } = api.comparison.deleteComparisons.useMutation({
		onSuccess: () => {
			m.success("Порівняння видалено");
			refetch();
			router.push(RoutesConfig.Home);
		},
		onError: (error) => {
			m.error(error.message);
		},
	});

	useEffect(() => {
		if (comparisons.length !== 2 && !isLoading) {
			router.push(RoutesConfig.Home);
		}
	}, [comparisons, isLoading, router]);

	if (isLoading || comparisons.length !== 2) {
		return <></>;
	}

	const handleDeleteComparisons = () => {
		DeleteComparisons();
	};

	const comparison1 = comparisons[0]!;
	const comparison2 = comparisons[1]!;

	return (
		<>
			<h2 className="my-10 text-center text-2xl font-semibold text-gray">
				Порівняння меблевих товарів
			</h2>
			<ComparisonTable
				comparison1={comparison1}
				comparison2={comparison2}
				handleDeleteComparisons={handleDeleteComparisons}
			/>
		</>
	);
};
ComparisonPage.getLayout = (page) => <AppLayout>{page}</AppLayout>;

export default ComparisonPage;

export const getServerSideProps: GetServerSideProps = async (
	context: GetServerSidePropsContext,
) => {
	const session = await getServerSession(context.req, context.res, authOptions);
	const accessCheck = checkAccess(session, context);

	if (accessCheck) {
		return accessCheck;
	}

	return {
		props: {},
	};
};
