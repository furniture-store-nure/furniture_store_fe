import { type FC } from "react";
import type { NextPage } from "next";
import { type Session } from "next-auth";
import styled from "@emotion/styled";
import { SessionProvider } from "next-auth/react";
import type { AppProps } from "next/app";
import { MaterialDesignContent, SnackbarProvider } from "notistack";
import { Provider } from "react-redux";

import { store } from "~/redux/store";
import { api } from "~/utils/api";
import { Icon } from "~/components/UI";

import "~/styles/globals.css";

const StyledMaterialDesignContent = styled(MaterialDesignContent)(() => ({
	"#notistack-snackbar": {
		padding: "0px",
		display: "flex",
		alignItems: "center",
		gap: "9px",
	},
	"&.notistack-MuiContent-success": {
		maxWidth: "700px",
		minHeight: "48px",
		padding: "12px 20px",
		backgroundColor: "#29F680",
		color: "#121218",
		borderRadius: "12px",
		fontSize: "16px",
		fontFamily: "Poppins",
		letterSpacing: "0.32px",
	},
	"&.notistack-MuiContent-error": {
		maxWidth: "700px",
		minHeight: "48px",
		padding: "12px 20px",
		backgroundColor: "#F3212A",
		color: "#FFFFFF",
		borderRadius: "12px",
		fontSize: "16px",
		fontFamily: "Poppins",
		letterSpacing: "0.32px",
	},
}));

export type NextPageWithLayout<
	P = {
		//
	},
	IP = P,
> = NextPage<P, IP> & {
	getLayout?: (page: React.ReactElement) => React.ReactNode;
};

type AppPropsWithLayout<T> = AppProps<T> & {
	Component: NextPageWithLayout;
};

const InnerApp = ({ Component, pageProps }: AppPropsWithLayout<any>) => {
	const getLayout = Component.getLayout ?? ((page) => page);

	return (
		<SnackbarProvider
			Components={{
				success: StyledMaterialDesignContent,
				error: StyledMaterialDesignContent,
			}}
			iconVariant={{
				success: (
					<Icon className="!h-fit !w-fit shrink-0" icon="check_mark_black" />
				),
				error: <Icon className="!h-fit !w-fit shrink-0" icon="cross_white" />,
			}}
			anchorOrigin={{
				vertical: "bottom",
				horizontal: "center",
			}}
		>
			{getLayout(<Component {...pageProps} />)}
		</SnackbarProvider>
	);
};

const App: FC<AppProps & { session: Session | null }> = (props) => {
	return (
		<Provider store={store}>
			<SessionProvider session={props.session}>
				<InnerApp {...props} />
			</SessionProvider>
		</Provider>
	);
};

export default api.withTRPC(App);
