import { useEffect, useState } from "react";
import type { GetServerSideProps, GetServerSidePropsContext } from "next";
import { getServerSession } from "next-auth";
import type { Security, Material, TypeProduct } from "@prisma/client";
import { useRouter } from "next/router";

import { type NextPageWithLayout } from "./_app";
import { authOptions } from "~/server/auth";
import { checkAccess } from "~/utils/ssr";
import { AppLayout } from "~/components/layouts";
import { useGetProductsByScroll } from "~/hooks/products";
import {
	PopularProducts,
	PriceFilters,
	ProductCards,
	SearchPanel,
} from "~/widgets/home";

const HomePage: NextPageWithLayout = () => {
	const router = useRouter();

	const queryManufacturerId = router.query.manufacturerId as string;
	const queryMaterial = router.query.material as Material;
	const queryType = router.query.type as TypeProduct;
	const querySecurity = router.query.security as Security;

	const [priceFilters, setPriceFilters] = useState<
		{ priceFrom?: number; priceTo?: number }[]
	>([]);
	const [manufacturerId, setManufacturerId] = useState<string | undefined>(
		undefined,
	);
	const [type, setType] = useState<TypeProduct | undefined>(undefined);
	const [material, setMaterial] = useState<Material | undefined>(undefined);
	const [security, setSecurity] = useState<Security | undefined>(undefined);

	const { products, count, isLoading, fetchNextPage, hasNextPage } =
		useGetProductsByScroll({
			manufacturerId,
			type,
			material,
			security,
			priceFilters,
		});

	const handleShowMore = async () => {
		if (hasNextPage && !isLoading) {
			await fetchNextPage();
		}
	};

	const handlePriceFilters = (
		filters: { priceFrom?: number; priceTo?: number }[],
	) => {
		setPriceFilters(filters);
	};

	const handleSearch = (
		manufacturer?: string,
		typeProduct?: TypeProduct,
		materialProduct?: Material,
		securityProduct?: Security,
	) => {
		setManufacturerId(manufacturer);
		setType(typeProduct);
		setMaterial(materialProduct);
		setSecurity(securityProduct);
	};

	useEffect(() => {
		setManufacturerId(queryManufacturerId);
		setType(queryType);
		setMaterial(queryMaterial);
		setSecurity(querySecurity);
	}, [queryManufacturerId, queryMaterial, querySecurity, queryType]);

	return (
		<div>
			<div className="mt-11 flex flex-col gap-y-11">
				<h2 className="text-center text-xl font-semibold text-gray">
					Найпопулярніші меблеві товари
				</h2>
				<PopularProducts />
				<SearchPanel
					queryManufacturerId={manufacturerId}
					queryType={type}
					queryMaterial={material}
					querySecurity={security}
					handleSearch={handleSearch}
				/>
			</div>
			<div className="mt-11 flex gap-x-6">
				<PriceFilters handlePriceFilters={handlePriceFilters} />
				<ProductCards
					count={count}
					products={products}
					hasNextPage={hasNextPage}
					handleShowMore={handleShowMore}
				/>
			</div>
		</div>
	);
};

HomePage.getLayout = (page) => <AppLayout>{page}</AppLayout>;

export default HomePage;

export const getServerSideProps: GetServerSideProps = async (
	context: GetServerSidePropsContext,
) => {
	const session = await getServerSession(context.req, context.res, authOptions);
	const accessCheck = checkAccess(session, context);

	if (accessCheck) {
		return accessCheck;
	}

	return {
		props: {},
	};
};
