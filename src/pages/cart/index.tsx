import { useEffect, useState } from "react";
import { type GetServerSideProps, type GetServerSidePropsContext } from "next";
import { getServerSession } from "next-auth";
import Image from "next/image";
import type { Product, Cart } from "@prisma/client";
import { useRouter } from "next/router";

import type { NextPageWithLayout } from "../_app";
import { AppLayout } from "~/components/layouts";
import { authOptions } from "~/server/auth";
import { checkAccess } from "~/utils/ssr";
import { useAuth, useMessage } from "~/hooks";
import { useGetCarts } from "~/hooks/carts";
import { Button, Input, Icon } from "~/components/UI";
import { api } from "~/utils/api";
import { selectUpdateRequest, setUpdateRequest } from "~/redux/updateRequest";
import { useAppDispatch, useAppSelector } from "~/redux/hooks";
import { RoutesConfig } from "~/config/routesConfig";

const CartPage: NextPageWithLayout = () => {
	const router = useRouter();
	const m = useMessage();
	const { userId } = useAuth();
	const { carts, refetch } = useGetCarts({ userId });

	const dispatch = useAppDispatch();
	const needsUpdate = useAppSelector(selectUpdateRequest);

	const [currentCarts, setCurrentCarts] = useState<
		(Cart & { product: Product })[]
	>([]);

	const { mutate: handleDeleteCart } = api.cart.deleteCart.useMutation({
		onSuccess: () => {
			refetch();
			dispatch(setUpdateRequest({ needsUpdate: [...needsUpdate, "countCarts"] }));
			m.success("Товар видалено з кошика");
		},
		onError: (error) => {
			m.error(error.message);
		},
	});

	const { mutate: handleChangeQuantity } = api.cart.updateCart.useMutation({
		onSuccess: () => {
			refetch();
		},
		onError: (error) => {
			m.error(error.message);
		},
	});

	const totalPrice = currentCarts.reduce(
		(acc, cart) => acc + cart.product.price * cart.quantity,
		0,
	);

	const handleQuantityChange = (newValue: string, cartId: string) => {
		let quantity = newValue;

		if (quantity === "") {
			quantity = "1";
		} else if (Number(quantity) > 100) {
			quantity = "100";
		}

		handleChangeQuantity({ cartId, quantity: Number(quantity) });
		const newCarts = currentCarts.map((c) => ({
			...c,
			quantity: c.id === cartId ? Number(quantity) : c.quantity,
		}));
		setCurrentCarts(newCarts);
	};

	const handleRemoveFromCart = (cartId: string) => {
		handleDeleteCart(cartId);
		setCurrentCarts(currentCarts.filter((c) => c.id !== cartId));
	};

	const handleCreateOrder = () => {
		router.push(RoutesConfig.CreateOrder);
	};

	useEffect(() => {
		if (carts.length > 0) {
			setCurrentCarts([...carts]);
		}
	}, [carts]);

	useEffect(() => {
		if (needsUpdate.includes("cart")) {
			refetch();
			dispatch(
				setUpdateRequest({
					needsUpdate: needsUpdate.filter((u) => u !== "cart"),
				}),
			);
		}
	}, [dispatch, needsUpdate, refetch]);

	return (
		<div className="mt-[100px]">
			<div className="card w-full rounded-2xl bg-white shadow">
				{currentCarts.length > 0 ? (
					<table className="w-full">
						<thead className="h-16 border-b-4 border-grayMedium">
							<tr>
								<th className="w-1/3 pl-6 text-left">
									<span className="flex gap-x-2">
										<Icon icon="cart" />
										<span className="text-xl font-semibold text-gray">
											Кошик
										</span>
									</span>
								</th>
								<th className="w-1/3 text-left text-xl font-semibold text-gray">
									Кількість
								</th>
								<th className="w-[120px] text-left text-xl font-semibold text-gray">
									Ціна
								</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							{currentCarts.map((cart) => (
								<tr
									key={cart.id}
									className="group table-row border-b-2 border-grayMedium"
								>
									<td className="table-cell w-1/3 py-4 pl-6">
										<div className="flex items-center gap-x-6">
											<Image
												className="h-[90px] w-[140px] rounded-2xl"
												src={cart.product.logo}
												width={140}
												height={90}
												alt={cart.product.name}
											/>
											<span className="text-xl font-semibold text-gray">
												{cart.product.name}
											</span>
										</div>
									</td>
									<td className="table-cell w-1/3">
										<Input
											type="number"
											min="1"
											max={100}
											className="w-[200px]"
											name={`quantity-${cart.id}`}
											variant="numeric"
											value={cart.quantity.toString()}
											onChange={(e) =>
												handleQuantityChange(e.target.value, cart.id)
											}
										/>
									</td>
									<td className="table-cell w-[120px]">
										<span className="text-2xl font-semibold text-red">
											{(cart.product.price * cart.quantity).toLocaleString(
												"ru-RU",
											)}{" "}
											₴
										</span>
									</td>
									<td className="table-cell pr-9 text-right">
										<button onClick={() => handleRemoveFromCart(cart.id)}>
											<Icon icon="cross" />
										</button>
									</td>
								</tr>
							))}
							<tr>
								<td className="table-cell"></td>
								<td className="table-cell"></td>
								<td className="table-cell w-[120px]">
									<span className="text-2xl font-semibold text-red">
										{totalPrice.toLocaleString("ru-RU")} ₴
									</span>
								</td>
								<td className="table-cell py-7">
									<Button
										className="h-9"
										variant="primary"
										onClick={handleCreateOrder}
									>
										Оформити замовлення
									</Button>
								</td>
							</tr>
						</tbody>
					</table>
				) : (
					<div className="flex h-[300px] items-center justify-center">
						<span className="text-xl font-semibold text-gray">
							Кошик порожній
						</span>
					</div>
				)}
			</div>
		</div>
	);
};

CartPage.getLayout = (page) => <AppLayout>{page}</AppLayout>;

export default CartPage;

export const getServerSideProps: GetServerSideProps = async (
	context: GetServerSidePropsContext,
) => {
	const session = await getServerSession(context.req, context.res, authOptions);
	const accessCheck = checkAccess(session, context);

	if (accessCheck) {
		return accessCheck;
	}

	return {
		props: {},
	};
};
