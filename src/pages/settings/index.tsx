import { useState } from "react";
import { type GetServerSideProps, type GetServerSidePropsContext } from "next";
import { getServerSession } from "next-auth";

import type { NextPageWithLayout } from "../_app";
import { AppLayout } from "~/components/layouts";
import { checkAccess } from "~/utils/ssr";
import { authOptions } from "~/server/auth";
import { SettingsTab } from "~/utils/constants";
import { Icon } from "~/components/UI";
import {
	SettingsEmail,
	SettingsInfo,
	SettingsPassword,
	SettingsTabs,
} from "~/widgets/settings";

const SettingsPage: NextPageWithLayout = () => {
	const [activeTab, setActiveTab] = useState<SettingsTab>(SettingsTab.Info);

	return (
		<div className="mx-auto mt-8 flex flex-col gap-y-10">
			<span className="flex items-center gap-x-2 pl-6">
				<Icon icon="person" />
				<h2 className="text-xl font-semibold text-gray">Особистий кабінет</h2>
			</span>
			<SettingsTabs activeTab={activeTab} changeActiveTab={setActiveTab} />
			{activeTab === SettingsTab.Info && <SettingsInfo />}
			{activeTab === SettingsTab.Password && <SettingsPassword />}
			{activeTab === SettingsTab.Email && <SettingsEmail />}
		</div>
	);
};

SettingsPage.getLayout = (page) => <AppLayout>{page}</AppLayout>;

export default SettingsPage;

export const getServerSideProps: GetServerSideProps = async (
	context: GetServerSidePropsContext,
) => {
	const session = await getServerSession(context.req, context.res, authOptions);
	const accessCheck = checkAccess(session, context);

	if (accessCheck) {
		return accessCheck;
	}

	return {
		props: {},
	};
};
