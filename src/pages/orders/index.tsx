import { useCallback, useEffect, useState } from "react";
import { type GetServerSideProps, type GetServerSidePropsContext } from "next";
import { useRouter } from "next/router";
import { getServerSession } from "next-auth";

import { checkAccess } from "~/utils/ssr";
import { authOptions } from "~/server/auth";
import { AppLayout } from "~/components/layouts";
import type { NextPageWithLayout } from "../_app";
import { Table } from "~/components/UI";
import {
	type OrderTableColumn,
	useOrderColumns,
	useGetOrders,
} from "~/hooks/orders";
import { useAppDispatch, useAppSelector } from "~/redux/hooks";
import { selectUpdateRequest, setUpdateRequest } from "~/redux/updateRequest";

const LIMIT = 10;

const OrdersPage: NextPageWithLayout = () => {
	const dispatch = useAppDispatch();
	const router = useRouter();

	const queryPage = router.query.page ? Number(router.query.page) : 1;

	const [page, setPage] = useState<number>(queryPage);

	const handlePageChange = (page: number) => {
		setPage(page);
		router.replace(`?page=${page}`);
	};

	const needsUpdate = useAppSelector(selectUpdateRequest);
	const { orders, count, refetch } = useGetOrders({ page, limit: LIMIT });

	const handleRefetch = useCallback(() => {
		refetch();
	}, [refetch]);

	const { ordersColumns } = useOrderColumns({ handleRefetch });

	useEffect(() => {
		if (needsUpdate.includes("orders")) {
			handleRefetch();
			dispatch(
				setUpdateRequest({
					needsUpdate: needsUpdate.filter((u) => u !== "orders"),
				}),
			);
		}
	}, [dispatch, needsUpdate, handleRefetch]);

	return (
		<>
			<div className="flex flex-col">
				<h2 className="my-10 ml-[30px] text-2xl font-semibold text-gray">
					Замовлення
				</h2>
				{orders.length > 0 ? (
					<Table<OrderTableColumn>
						rowName="Замовлення"
						columns={ordersColumns}
						rows={orders}
						totalCount={count}
						limit={LIMIT}
						onChangePage={handlePageChange}
					/>
				) : (
					<div className="flex h-[300px] items-center justify-center rounded-2xl bg-white shadow">
						<span className="text-xl font-semibold text-gray">
							Замовлення порожні
						</span>
					</div>
				)}
			</div>
		</>
	);
};

OrdersPage.getLayout = (page) => <AppLayout>{page}</AppLayout>;

export default OrdersPage;

export const getServerSideProps: GetServerSideProps = async (
	context: GetServerSidePropsContext,
) => {
	const session = await getServerSession(context.req, context.res, authOptions);
	const accessCheck = checkAccess(session, context);

	if (accessCheck) {
		return accessCheck;
	}

	return {
		props: {},
	};
};
