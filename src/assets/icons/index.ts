import add_box from "~/assets/icons/add_box.svg";
import attach_file from "~/assets/icons/attach_file.svg";
import bed_size from "~/assets/icons/bed_size.svg";
import bed_size_gray from "~/assets/icons/bed_size_gray.svg";
import cart from "~/assets/icons/cart.svg";
import child from "~/assets/icons/child.svg";
import child_gray from "~/assets/icons/child_gray.svg";
import check_mark_black from "~/assets/icons/check_mark_black.svg";
import check from "~/assets/icons/check.svg";
import cross_white from "~/assets/icons/cross_white.svg";
import cross from "~/assets/icons/cross.svg";
import down_arrow_green from "~/assets/icons/down_arrow_green.svg";
import location from "~/assets/icons/location.svg";
import login from "~/assets/icons/login.svg";
import logout from "~/assets/icons/logout.svg";
import mail_gray from "~/assets/icons/mail_gray.svg";
import mail from "~/assets/icons/mail.svg";
import material_gray from "~/assets/icons/material_gray.svg";
import material from "~/assets/icons/material.svg";
import pen from "~/assets/icons/pen.svg";
import person_gray from "~/assets/icons/person_gray.svg";
import person from "~/assets/icons/person.svg";
import phone_gray from "~/assets/icons/phone_gray.svg";
import phone from "~/assets/icons/phone.svg";
import place from "~/assets/icons/place.svg";
import trash from "~/assets/icons/trash.svg";
import up_arrow_green from "~/assets/icons/up_arrow_green.svg";
import schedule from "~/assets/icons/schedule.svg";
import scales_gray from "~/assets/icons/scales_gray.svg";
import scales from "~/assets/icons/scales.svg";
import vertical_divider from "~/assets/icons/vertical_divider.svg";
import weight from "~/assets/icons/weight.svg";
import left_arrow from "~/assets/icons/left_arrow.svg";
import right_arrow from "~/assets/icons/right_arrow.svg";

export const icons = {
	add_box,
	attach_file,
	bed_size,
	bed_size_gray,
	cart,
	child,
	child_gray,
	check_mark_black,
	check,
	cross_white,
	cross,
	down_arrow_green,
	location,
	login,
	logout,
	mail_gray,
	mail,
	material_gray,
	material,
	pen,
	person_gray,
	person,
	phone_gray,
	phone,
	place,
	trash,
	up_arrow_green,
	schedule,
	scales_gray,
	scales,
	vertical_divider,
	weight,
	left_arrow,
	right_arrow,
};
