"use client";
import { useRouter } from "next/navigation";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { type z } from "zod";

import { RoutesConfig } from "~/config/routesConfig";
import { useMessage } from "~/hooks";
import { Button, ControlledInput, Form, Label, Modal } from "~/components/UI";
import { signInSchema } from "~/server/core/validations";
import { signIn } from "next-auth/react";
import { useCallback } from "react";

type FormValues = z.input<typeof signInSchema>;

export const SignIn = () => {
	const m = useMessage();
	const router = useRouter();

	const form = useForm<FormValues>({
		mode: "onChange",
		resolver: zodResolver(signInSchema),
	});

	const {
		formState: { errors, isValid },
	} = form;

	const handleRegistration = () => {
		router.push(RoutesConfig.Registration);
	};

	const handleSubmit = useCallback(
		(data: FormValues) => {
			signIn("credentials", {
				redirect: false,
				email: data.email,
				password: data.password,
				callbackUrl:
					window !== undefined
						? `${window.location.origin}${RoutesConfig.Home}`
						: "",
			})
				.then((response) => {
					if (response?.ok) {
						if (response.url) {
							void router.push(response.url);
						} else {
							console.error("Response URL is null");
						}
					} else m.error(response?.error ?? "Invalid credentials");
				})
				.catch((error) => {
					m.error("Invalid credentials");
					console.error("error", error);
				});
		},
		[m, router],
	);

	return (
		<Modal
			title="Авторизація"
			subTitle="Щоб увійти як користувач, заповніть поля нижче"
		>
			<Form
				form={form}
				className="flex w-full flex-col items-center gap-y-5"
				onSubmit={handleSubmit}
			>
				<div className="flex w-full items-center justify-between">
					<Label htmlFor="email">Ел. пошта</Label>
					<ControlledInput
						name="email"
						placeholder="Твоя ел. пошта"
						errorMessage={errors.email?.message}
					/>
				</div>
				<div className="flex w-full items-center justify-between">
					<Label htmlFor="password">Пароль</Label>
					<ControlledInput
						name="password"
						type="password"
						placeholder="Твій пароль"
						errorMessage={errors.password?.message}
					/>
				</div>
				<Button type="submit" disabled={!isValid}>
					Увійти
				</Button>
			</Form>

			<span className="flex">
				<p className="text-sm font-semibold tracking-wide text-turquoise">
					У вас немає аккаунту?
				</p>
				<a
					className="cursor-pointer text-sm tracking-wide text-grayMedium2 hover:underline"
					onClick={handleRegistration}
				>
					Зареєструватися
				</a>
			</span>
		</Modal>
	);
};
