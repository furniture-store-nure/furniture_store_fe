import Image from "next/image";
import { useRouter } from "next/router";

import { api } from "~/utils/api";
import { Button, Icon } from "~/components/UI";
import { RoutesConfig } from "~/config/routesConfig";
import { useAuth, useMessage } from "~/hooks";
import { useAppDispatch, useAppSelector } from "~/redux/hooks";
import { selectUpdateRequest, setUpdateRequest } from "~/redux/updateRequest";

type PopularProductCardProps = {
	isComparison?: boolean;
	activeComparison?: boolean;
	productId: string;
	logo: string;
	name: string;
	price: number;
	handleRefetchComparisons: () => void;
};

export const PopularProductCard = (props: PopularProductCardProps) => {
	const {
		productId,
		logo,
		name,
		price,
		isComparison = false,
		activeComparison = false,
		handleRefetchComparisons,
	} = props;

	const router = useRouter();
	const { userId, isAdmin, session } = useAuth();
	const m = useMessage();
	const dispatch = useAppDispatch();
	const needsUpdate = useAppSelector(selectUpdateRequest);

	const { mutate: handleCreateCart } = api.cart.createCart.useMutation({
		onSuccess: () => {
			dispatch(
				setUpdateRequest({
					needsUpdate: [...needsUpdate, "cart", "countCarts"],
				}),
			);
			m.success("Товар додано до кошику");
		},
		onError: (error) => {
			m.error(error.message);
		},
	});

	const { mutate: handleCreateComparison } =
		api.comparison.createComparison.useMutation({
			onSuccess: () => {
				m.success("Товар додано до порівняння");
				dispatch(
					setUpdateRequest({ needsUpdate: [...needsUpdate, "comparisons"] }),
				);
				handleRefetchComparisons();
			},
			onError: (error) => {
				m.error(error.message);
			},
		});

	const handleAddToComparison = () => {
		if (!session) {
			return router.push(RoutesConfig.Login);
		}

		handleCreateComparison(productId);
	};

	const handleComparison = () => {
		if (!session) {
			return router.push(RoutesConfig.Login);
		}
		router.push(RoutesConfig.Comparison);
	};

	const handleAddToCart = () => {
		if (!session) {
			return router.push(RoutesConfig.Login);
		}

		handleCreateCart({
			productId: productId,
			userId: userId,
		});
	};

	return (
		<div className="flex-1 shrink-0 rounded-2xl bg-white shadow-card">
			<div className="relative flex items-center justify-center">
				<div className="absolute left-0 top-0 flex h-[50px] w-[100px] items-center justify-center rounded-br-2xl rounded-tl-2xl bg-red">
					<h2 className="text-center text-xl font-semibold text-white">ТОП</h2>
				</div>
				<Image
					className="h-[300px] w-[437px] rounded-t-2xl"
					src={logo}
					alt={name}
					width={437}
					height={300}
				/>
			</div>
			<div className="flex flex-col p-6">
				<h3 className="text-xl font-semibold text-gray">{name}</h3>
				<div className="mt-8 flex shrink-0 items-center justify-between">
					<p className="shrink-0 text-2xl font-semibold text-red">
						{price.toLocaleString("ru-RU")} ₴
					</p>
					<div className="relative flex items-center">
						{!isAdmin && (
							<button
								disabled={isComparison || activeComparison}
								onClick={handleAddToComparison}
							>
								<Icon icon={isComparison ? "scales" : "scales_gray"} />
							</button>
						)}
						{activeComparison && isComparison && (
							<button
								onClick={handleComparison}
								className="absolute bottom-4 left-5 z-10 flex h-12 w-36 items-center justify-center rounded-2xl border-2 border-turquoise bg-white text-center text-base font-semibold text-gray"
							>
								Порівняти
							</button>
						)}
					</div>

					{!isAdmin && (
						<Button
							className="h-9 text-sm"
							variant="primary"
							onClick={handleAddToCart}
						>
							Додати до кошику
						</Button>
					)}
				</div>
			</div>
		</div>
	);
};
