import type { Manufacturer, Product } from "@prisma/client";

import { ProductCard } from "./ProductCard";
import { Button } from "~/components/UI";
import { useGetComparisons } from "~/hooks/comparison";

type ProductCardsProps = {
	count: number;
	products: (Product & { manufacturer: Manufacturer })[];
	hasNextPage: boolean;
	handleShowMore: () => void;
};

export const ProductCards = (props: ProductCardsProps) => {
	const { count, products, hasNextPage, handleShowMore } = props;

	const { comparisons, refetch } = useGetComparisons();

	const handleRefetchComparisons = () => {
		refetch();
	};

	const activeComparison = comparisons.length === 2;

	return (
		<div className="flex flex-col items-center">
			<h2 className="mb-5 self-start text-base tracking-wider text-grayMedium2">
				Усього знайдено: {count} меблів
			</h2>
			<div className="grid w-full grid-cols-2 gap-x-16 gap-y-16">
				{products.map((product) => (
					<ProductCard
						key={product.id}
						isComparison={comparisons.some((comparison) => comparison.productId === product.id)}
						activeComparison={activeComparison}
						productId={product.id}
						security={product.security}
						logo={product.logo}
						name={product.name}
						manufacturer={product.manufacturer.name}
						price={product.price}
						material={product.material}
						productType={product.type}
						height={product.height}
						width={product.width}
						weight={product.weight}
						handleRefetchComparisons={handleRefetchComparisons}
					/>
				))}
			</div>
			{hasNextPage && (
				<Button className="mt-20" variant="secondary" onClick={handleShowMore}>
					Показати більше
				</Button>
			)}
		</div>
	);
};
