import { useGetPopularProducts } from "~/hooks/products";
import { PopularProductCard } from "./PopularProductCard";
import { useGetComparisons } from "~/hooks/comparison";

export const PopularProducts = () => {
	const { popularProducts } = useGetPopularProducts();
	const { comparisons, refetch } = useGetComparisons();

	const handleRefetchComparisons = () => {
		refetch();
	};

	const activeComparison = comparisons.length === 2;

	return (
		<div className="flex w-full gap-x-5">
			{popularProducts.map((product) => (
				<PopularProductCard
					key={product.id}
					productId={product.id}
					logo={product.logo}
					name={product.name}
					price={product.price}
					handleRefetchComparisons={handleRefetchComparisons}
					activeComparison={activeComparison}
					isComparison={comparisons.some(
						(comparison) => comparison.productId === product.id,
					)}
				/>
			))}
		</div>
	);
};
