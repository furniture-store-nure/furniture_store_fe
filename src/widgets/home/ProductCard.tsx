import Image from "next/image";
import { useRouter } from "next/router";

import { api } from "~/utils/api";
import { Button, Icon } from "~/components/UI";
import { RoutesConfig } from "~/config/routesConfig";
import { useAuth, useMessage } from "~/hooks";
import { useAppDispatch, useAppSelector } from "~/redux/hooks";
import { selectUpdateRequest, setUpdateRequest } from "~/redux/updateRequest";
import {
	MATERIAL_OPTIONS,
	PRODUCT_TYPE_OPTIONS,
	SECURITY_OPTIONS,
} from "~/utils/constants";

type ProductCardProps = {
	isComparison?: boolean;
	activeComparison?: boolean;
	productId: string;
	logo: string;
	name: string;
	manufacturer: string;
	security: string;
	price: number;
	material: string;
	productType: string;
	height: string;
	width: string;
	weight: string;
	handleRefetchComparisons: () => void;
};

export const ProductCard = (props: ProductCardProps) => {
	const {
		isComparison = false,
		activeComparison = false,
		productId,
		logo,
		name,
		manufacturer,
		security,
		price,
		material,
		productType,
		height,
		width,
		weight,
		handleRefetchComparisons,
	} = props;

	const router = useRouter();
	const { userId, isAdmin, session } = useAuth();
	const m = useMessage();
	const dispatch = useAppDispatch();
	const needsUpdate = useAppSelector(selectUpdateRequest);

	const { mutate: handleCreateCart } = api.cart.createCart.useMutation({
		onSuccess: () => {
			m.success("Товар додано до кошику");
			dispatch(
				setUpdateRequest({
					needsUpdate: [...needsUpdate, "cart", "countCarts"],
				}),
			);
		},
		onError: (error) => {
			m.error(error.message);
		},
	});

	const { mutate: handleCreateComparison } =
		api.comparison.createComparison.useMutation({
			onSuccess: () => {
				m.success("Товар додано до порівняння");
				dispatch(
					setUpdateRequest({ needsUpdate: [...needsUpdate, "comparisons"] }),
				);
				handleRefetchComparisons();
			},
			onError: (error) => {
				m.error(error.message);
			},
		});

	const handleAddToCart = () => {
		if (!session) {
			return router.push(RoutesConfig.Login);
		}

		handleCreateCart({
			productId: productId,
			userId: userId,
		});
	};

	const handleAddToComparison = () => {
		if (!session) {
			return router.push(RoutesConfig.Login);
		}

		handleCreateComparison(productId);
	};

	const handleComparison = () => {
		if (!session) {
			return router.push(RoutesConfig.Login);
		}
		router.push(RoutesConfig.Comparison);
	};

	const productLabel = PRODUCT_TYPE_OPTIONS.find(
		(option) => option.value === productType,
	)?.label;

	const materialLabel = MATERIAL_OPTIONS.find(
		(option) => option.value === material,
	)?.label;

	const securityLabel = SECURITY_OPTIONS.find(
		(option) => option.value === security,
	)?.label;

	return (
		<div className="flex-1 shrink-0 rounded-2xl bg-white shadow-card">
			<div className="flex items-center justify-center">
				<Image
					className="h-[300px] w-[437px] rounded-t-2xl"
					src={logo}
					alt={name}
					width={437}
					height={300}
				/>
			</div>
			<div className="flex flex-col p-6">
				<h3 className="text-xl font-semibold text-gray">{name}</h3>
				<p className="text-base tracking-wider text-grayMedium2">
					{manufacturer}
				</p>
				<div className="mt-2 text-base tracking-wider text-grayMedium2">
					<div className="flex items-center gap-x-2">
						<Icon icon="person_gray" />
						<p>{productLabel}</p>
					</div>
					<div className="flex items-center gap-x-2">
						<Icon icon="bed_size_gray" />
						<p>
							{width}x{height}
						</p>
					</div>
					<div className="flex items-center gap-x-2">
						<Icon icon="material_gray" />
						<p>{materialLabel}</p>
					</div>
					<div className="flex items-center gap-x-2">
						<Icon icon="weight" />
						<p>{weight} кг</p>
					</div>
					<div className="flex items-center gap-x-2">
						<Icon icon="child_gray" />
						<p>{securityLabel}</p>
					</div>
				</div>
				<div className="mt-8 flex shrink-0 items-center justify-between">
					<p className="shrink-0 text-2xl font-semibold text-red">
						{price.toLocaleString("ru-RU")} ₴
					</p>
					<div className="relative flex items-center">
						{!isAdmin && (
							<button
								disabled={isComparison || activeComparison}
								onClick={handleAddToComparison}
							>
								<Icon icon={isComparison ? "scales" : "scales_gray"} />
							</button>
						)}
						{activeComparison && isComparison && (
							<button
								onClick={handleComparison}
								className="absolute bottom-4 left-5 z-10 flex h-12 w-36 items-center justify-center rounded-2xl border-2 border-turquoise bg-white text-center text-base font-semibold text-gray"
							>
								Порівняти
							</button>
						)}
					</div>

					{!isAdmin && (
						<Button
							className="h-9 text-sm"
							variant="primary"
							onClick={handleAddToCart}
						>
							Додати до кошику
						</Button>
					)}
				</div>
			</div>
		</div>
	);
};
