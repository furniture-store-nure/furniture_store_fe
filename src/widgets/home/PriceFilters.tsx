import { useEffect } from "react";
import { useForm } from "react-hook-form";
import { z } from "zod";

import { ControlledCheckbox, Form } from "~/components/UI";

const formSchema = z.object({
	priceTo5000: z.number().optional(),
	priceFrom5000to10000: z.boolean(),
	priceFrom10000to15000: z.boolean(),
	priceFrom15000to20000: z.boolean(),
	priceFrom20000to25000: z.boolean(),
	priceFrom25000to30000: z.boolean(),
	priceFrom30000: z.boolean(),
});

type FormValues = z.input<typeof formSchema>;

type PriceFiltersProps = {
	handlePriceFilters: (
		priceFilters: { priceFrom?: number; priceTo?: number }[],
	) => void;
};

export const PriceFilters = (props: PriceFiltersProps) => {
	const { handlePriceFilters } = props;

	const form = useForm<FormValues>({ mode: "onChange" });

	// Use useWatch inside useEffect
	useEffect(() => {
		const subscription = form.watch((values) => {
			const priceFilters = [];

			if (values.priceTo5000) {
				priceFilters.push({ priceTo: 5000 });
			}

			if (values.priceFrom5000to10000) {
				priceFilters.push({ priceFrom: 5000, priceTo: 10000 });
			}

			if (values.priceFrom10000to15000) {
				priceFilters.push({ priceFrom: 10000, priceTo: 15000 });
			}

			if (values.priceFrom15000to20000) {
				priceFilters.push({ priceFrom: 15000, priceTo: 20000 });
			}

			if (values.priceFrom20000to25000) {
				priceFilters.push({ priceFrom: 20000, priceTo: 25000 });
			}

			if (values.priceFrom25000to30000) {
				priceFilters.push({ priceFrom: 25000, priceTo: 30000 });
			}

			if (values.priceFrom30000) {
				priceFilters.push({ priceFrom: 30000 });
			}

			handlePriceFilters(priceFilters);
		});
		return () => subscription.unsubscribe();
	}, [form, handlePriceFilters]);

	return (
		<Form<FormValues>
			form={form}
			className="h-fit w-[348px] rounded-2xl bg-white p-4 shadow-card"
		>
			<h2 className="font-semibold tracking-wider text-gray">Цiна</h2>
			<div className="mt-7 flex flex-col gap-4">
			<div className="flex items-center">
					<ControlledCheckbox
						name="priceTo5000"
						label="До ₴ 5 000"
					/>
				</div>
				<div className="flex items-center">
					<ControlledCheckbox
						name="priceFrom5000to10000"
						label="₴ 5 000 - ₴ 10 000"
					/>
				</div>
				<div className="flex items-center">
					<ControlledCheckbox
						name="priceFrom10000to15000"
						label="₴ 10 000 - ₴ 15 000"
					/>
				</div>
				<div className="flex items-center">
					<ControlledCheckbox
						name="priceFrom15000to20000"
						label="₴ 15 000 - ₴ 20 000"
					/>
				</div>
				<div className="flex items-center">
					<ControlledCheckbox
						name="priceFrom20000to25000"
						label="₴ 20 000 - ₴ 25 000"
					/>
				</div>
				<div className="flex items-center">
					<ControlledCheckbox
						name="priceFrom25000to30000"
						label="₴ 25 000 - ₴ 30 000"
					/>
				</div>
				<div className="flex items-center">
					<ControlledCheckbox name="priceFrom30000" label="₴ 30 000 +" />
				</div>
			</div>
		</Form>
	);
};
