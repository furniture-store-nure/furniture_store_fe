import { useEffect } from "react";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { useRouter } from "next/router";
import { Material, Security, TypeProduct } from "@prisma/client";
import { z } from "zod";

import { useGetManufacturers } from "~/hooks/products";
import {
	MATERIAL_OPTIONS,
	PRODUCT_TYPE_OPTIONS,
	SECURITY_OPTIONS,
} from "~/utils/constants";
import { RoutesConfig } from "~/config/routesConfig";
import { Button, ControlledSelect, Form } from "~/components/UI";

const selectOptionSchema = z.object({
	value: z.string(),
	label: z.string(),
});

const selectOptionTypeSchema = z.object({
	value: z.nativeEnum(TypeProduct),
	label: z.string(),
});

const selectOptionMaterialSchema = z.object({
	value: z.nativeEnum(Material),
	label: z.string(),
});

const selectOptionSecuritySchema = z.object({
	value: z.nativeEnum(Security),
	label: z.string(),
});

const formSchema = z.object({
	manufacturer: selectOptionSchema.nullable(),
	type: selectOptionTypeSchema.nullable(),
	material: selectOptionMaterialSchema.nullable(),
	security: selectOptionSecuritySchema.nullable(),
});

type FormValues = z.infer<typeof formSchema>;

type SearchPanelProps = {
	queryManufacturerId?: string;
	queryType?: TypeProduct;
	queryMaterial?: Material;
	querySecurity?: Security;
	handleSearch: (
		manufacturerId?: string,
		type?: TypeProduct,
		material?: Material,
		security?: Security,
	) => void;
};

export const SearchPanel = (props: SearchPanelProps) => {
	const {
		queryManufacturerId,
		queryType,
		queryMaterial,
		querySecurity,
		handleSearch,
	} = props;

	const { manufacturers } = useGetManufacturers();

	const router = useRouter();
	const form = useForm<FormValues>({
		mode: "onChange",
		resolver: zodResolver(formSchema),
		defaultValues: {
			manufacturer: null,
			type: null,
			material: null,
			security: null,
		},
	});

	const handleSubmit = () => {
		const { manufacturer, type, material, security } = form.getValues();

		router.replace({
			pathname: RoutesConfig.Home,
			query: {
				...(manufacturer && { manufacturerId: manufacturer?.value }),
				...(type && { type: type?.value }),
				...(material && { material: material?.value }),
				...(security && { security: security?.value }),
			},
		});

		handleSearch(
			manufacturer?.value,
			type?.value,
			material?.value,
			security?.value,
		);
	};

	const handleReset = () => {
		form.reset();
		router.replace(RoutesConfig.Home);
		handleSearch();
	};

	useEffect(() => {
		if (manufacturers.length) {
			form.reset({
				manufacturer: manufacturers.find(
					(option) => option.value === queryManufacturerId,
				),
				type: PRODUCT_TYPE_OPTIONS.find((option) => option.value === queryType),
				material: MATERIAL_OPTIONS.find(
					(option) => option.value === queryMaterial,
				),
				security: SECURITY_OPTIONS.find(
					(option) => option.value === querySecurity,
				),
			});
		}
	}, [
		form,
		manufacturers,
		queryManufacturerId,
		queryMaterial,
		querySecurity,
		queryType,
	]);

	const formValues = form.watch();
	const isFormChanged =
		(!queryManufacturerId && !queryType && !queryMaterial) ||
		formValues.manufacturer?.value !== queryManufacturerId ||
		formValues.type?.value !== queryType ||
		formValues.material?.value !== queryMaterial ||
		formValues.security?.value !== querySecurity;

	return (
		<Form<FormValues>
			form={form}
			onSubmit={handleSubmit}
			className="flex h-full w-full rounded-2xl bg-white shadow-card"
		>
			<div className="flex flex-1 flex-col gap-y-6 border-r-2 border-beige p-5">
				<p className="text-sm tracking-wider text-grayMedium">Виробник</p>
				<ControlledSelect
					name="manufacturer"
					className="!h-fit w-full !p-0"
					dropDownClassName="top-10"
					startIcon="location"
					placeholder="Вибрати виробника"
					variant="outlined"
					options={manufacturers}
				/>
			</div>
			<div className="flex flex-1 flex-col gap-y-6 border-r-2 border-beige p-5">
				<p className="text-sm tracking-wider text-grayMedium">Матерiал</p>
				<ControlledSelect
					name="material"
					className="!h-fit w-full !p-0"
					dropDownClassName="top-10"
					startIcon="material"
					placeholder="Вибрати матерiал"
					variant="outlined"
					options={MATERIAL_OPTIONS}
				/>
			</div>
			<div className="flex flex-1 flex-col gap-y-6 border-r-2 border-beige p-5">
				<p className="text-sm tracking-wider text-grayMedium">Вид продукту</p>
				<ControlledSelect
					name="type"
					className="!h-fit w-full !p-0"
					dropDownClassName="top-10"
					startIcon="person"
					placeholder="Вибрати вид"
					variant="outlined"
					options={PRODUCT_TYPE_OPTIONS}
				/>
			</div>
			<div className="flex flex-1 flex-col gap-y-6 border-r-2 border-beige p-5">
				<p className="text-sm tracking-wider text-grayMedium">Безпека</p>
				<ControlledSelect
					name="security"
					className="!h-fit w-full !p-0"
					dropDownClassName="top-10"
					startIcon="child"
					placeholder="Вибрати безпеку"
					variant="outlined"
					options={SECURITY_OPTIONS}
				/>
			</div>
			<div className="flex flex-col items-center justify-center p-5">
				{isFormChanged ? (
					<Button className="w-[160px]" type="submit" variant="primary">
						Шукати
					</Button>
				) : (
					<Button
						className="w-[160px]"
						onClick={handleReset}
						variant="secondary"
					>
						Очистити
					</Button>
				)}
			</div>
		</Form>
	);
};
