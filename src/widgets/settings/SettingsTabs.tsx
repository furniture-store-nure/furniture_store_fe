import clsx from "classnames";

import { SettingsTab } from "~/utils/constants";

type SettingsTabsProps = {
	activeTab: SettingsTab;
	changeActiveTab: (tab: SettingsTab) => void;
};

export const SettingsTabs = (props: SettingsTabsProps) => {
	const { activeTab, changeActiveTab } = props;

	const handleTabClick = (tab: SettingsTab) => {
		changeActiveTab(tab);
	};

	return (
		<div className="flex h-9">
			<button
				className={clsx("w-[210px] px-2 text-center text-base tracking-wider", {
					"border border-transparent bg-turquoise font-semibold text-gray":
						activeTab === SettingsTab.Info,
					"border border-grayMedium text-grayMedium2":
						activeTab !== SettingsTab.Info,
				})}
				onClick={() => handleTabClick(SettingsTab.Info)}
			>
				Особиста iнформацiя
			</button>
			<button
				className={clsx("w-[140px] px-2 text-center text-base tracking-wider", {
					"border border-transparent bg-turquoise font-semibold text-gray":
						activeTab === SettingsTab.Password,
					"border border-grayMedium text-grayMedium2":
						activeTab !== SettingsTab.Password,
				})}
				onClick={() => handleTabClick(SettingsTab.Password)}
			>
				Зміна пароля
			</button>
			<button
				className={clsx("w-[140px] px-2 text-center text-base tracking-wider", {
					"border border-transparent bg-turquoise font-semibold text-gray":
						activeTab === SettingsTab.Email,
					"border border-grayMedium text-grayMedium2":
						activeTab !== SettingsTab.Email,
				})}
				onClick={() => handleTabClick(SettingsTab.Email)}
			>
				Зміна пошти
			</button>
		</div>
	);
};
