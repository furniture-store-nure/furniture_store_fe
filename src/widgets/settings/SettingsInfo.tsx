import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";

import { useGetUserById } from "~/hooks/user";
import { api } from "~/utils/api";
import { useMessage } from "~/hooks";
import {
	type UpdateUserArgs,
	updateUserSchema,
} from "~/server/core/validations";
import { Button, ControlledInput, Form, Icon, Label } from "~/components/UI";

export const SettingsInfo = () => {
	const m = useMessage();
	const { user, refetch } = useGetUserById();
	const form = useForm<UpdateUserArgs>({
		mode: "onChange",
		resolver: zodResolver(updateUserSchema),
	});

	const { errors } = form.formState;

	const [isEdit, setIsEdit] = useState<boolean>(false);

	const handleChangeEditState = () => {
		setIsEdit((prev) => !prev);
	};

	const handleReset = () => {
		if (user) {
			form.reset({
				firstName: user.firstName,
				lastName: user.lastName,
				middleName: user.middleName,
			});
			handleChangeEditState();
		}
	};

	const { mutate: handleUpdateUser } = api.auth.updateUser.useMutation({
		onSuccess: () => {
			refetch();
			handleChangeEditState();
			m.success("Інформація оновлена");
		},
		onError: (error) => {
			m.error(error.message);
		},
	});

	const handleSubmit = (data: UpdateUserArgs) => {
		if (user) {
			handleUpdateUser({
				...data,
			});
		}
	};

	useEffect(() => {
		if (user) {
			form.reset({
				firstName: user.firstName,
				lastName: user.lastName,
				middleName: user.middleName,
			});
		}
	}, [form, user]);

	if (!user) {
		return null;
	}

	let render;

	if (isEdit) {
		const isDisabled = !form.formState.isValid;

		render = (
			<Form<UpdateUserArgs>
				form={form}
				onSubmit={handleSubmit}
				className="flex flex-col gap-y-5 p-6"
			>
				<div className="flex items-center">
					<Label className="w-[200px]" htmlFor="firstName">
						Ім&apos;я
					</Label>
					<ControlledInput
						name="firstName"
						errorMessage={errors.firstName?.message}
					/>
				</div>
				<div className="flex items-center">
					<Label className="w-[200px]" htmlFor="middleName">
						По-батькові
					</Label>
					<ControlledInput
						name="middleName"
						errorMessage={errors.middleName?.message}
					/>
				</div>
				<div className="flex items-center">
					<Label className="w-[200px]" htmlFor="lastName">
						Прізвище
					</Label>
					<ControlledInput
						name="lastName"
						errorMessage={errors.lastName?.message}
					/>
				</div>
				<div className="flex items-center justify-center gap-x-5">
					<Button disabled={isDisabled} className="h-10" type="submit">
						Змінити
					</Button>
					<Button
						className="h-10"
						variant="secondary"
						type="reset"
						onClick={handleReset}
					>
						Скасувати
					</Button>
				</div>
			</Form>
		);
	} else {
		render = (
			<div className="flex flex-col gap-y-10 p-6">
				<div className="flex items-center">
					<p className="w-[200px] text-base tracking-wider text-gray">
						Ім&apos;я
					</p>
					<p className="text-base tracking-wider text-grayMedium2">
						{user.firstName}
					</p>
				</div>
				<div className="flex items-center">
					<p className="w-[200px] text-base tracking-wider text-gray">
						По-батькові
					</p>
					<p className="text-base tracking-wider text-grayMedium2">
						{user.middleName}
					</p>
				</div>
				<div className="flex items-center">
					<p className="w-[200px] text-base tracking-wider text-gray">
						Прізвище
					</p>
					<p className="text-base tracking-wider text-grayMedium2">
						{user.lastName}
					</p>
				</div>
			</div>
		);
	}

	return (
		<div className="w-[700px] rounded-2xl bg-white shadow-card">
			<span className="flex items-center gap-x-5 py-4 pl-6">
				<h2 className="text-xl font-semibold text-gray">Особиста iнформацiя</h2>
				{!isEdit && (
					<button onClick={handleChangeEditState}>
						<Icon icon="pen" />
					</button>
				)}
			</span>
			<div className="border-b-4 border-grayMedium" />
			{render}
		</div>
	);
};
