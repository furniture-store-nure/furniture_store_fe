import { useEffect } from "react";
import { z } from "zod";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";

import { Button, ControlledInput, Form, Label } from "~/components/UI";
import { useAuth, useMessage } from "~/hooks";
import { api } from "~/utils/api";

const formSchema = z
	.object({
		oldPassword: z
			.string()
			.regex(
				new RegExp(/^(?=.*\d)(?:(?=.*[a-z])|(?=.*[A-Z])).{8,}$/),
				"Пароль повинен містити 8+ символів, принаймні одну цифру та принаймні одну літеру",
			),
		newPassword: z
			.string()
			.regex(
				new RegExp(/^(?=.*\d)(?:(?=.*[a-z])|(?=.*[A-Z])).{8,}$/),
				"Пароль повинен містити 8+ символів, принаймні одну цифру та принаймні одну літеру",
			),
		repeatPassword: z
			.string()
			.regex(
				new RegExp(/^(?=.*\d)(?:(?=.*[a-z])|(?=.*[A-Z])).{8,}$/),
				"Пароль повинен містити 8+ символів, принаймні одну цифру та принаймні одну літеру",
			),
	})
	.refine((data) => data.newPassword === data.repeatPassword, {
		message: "Паролі повинні збігатися",
	});

type FormValues = z.input<typeof formSchema>;

export const SettingsPassword = () => {
	const { logout } = useAuth();
	const m = useMessage();
	const form = useForm<FormValues>({
		mode: "onChange",
		resolver: zodResolver(formSchema),
	});

	const { errors } = form.formState;

	const { mutate: handleChangePassword } = api.auth.changePassword.useMutation({
		onSuccess: () => {
			m.success("Пароль оновлено");
			logout();
		},
		onError: (error) => {
			m.error(error.message);
		},
	});

	const handleSubmit = (data: FormValues) => {
		handleChangePassword({
			...data,
		});
	};

	useEffect(() => {
		const values = Object.values(errors);

		if (
			values.length === 1 &&
			values[0]?.message === "Паролі повинні збігатися"
		) {
			m.error("Паролі повинні збігатися");
		}
	}, [errors, m]);

	const isDisabled = !form.formState.isValid;

	return (
		<div className="w-[700px] rounded-2xl bg-white shadow-card">
			<span className="flex items-center gap-x-5 py-4 pl-6">
				<h2 className="text-xl font-semibold text-gray">Змiна пароля</h2>
			</span>
			<div className="border-b-4 border-grayMedium" />
			<Form<FormValues>
				form={form}
				onSubmit={handleSubmit}
				className="flex flex-col gap-y-5 p-6"
			>
				<div className="flex items-center">
					<Label className="w-[200px]" htmlFor="oldPassword">
						Старий пароль
					</Label>
					<ControlledInput
						type="password"
						name="oldPassword"
						placeholder="Введить старий пароль"
						errorMessage={errors.oldPassword?.message}
					/>
				</div>
				<div className="flex items-center">
					<Label className="w-[200px]" htmlFor="newPassword">
						Новий пароль
					</Label>
					<ControlledInput
						type="password"
						name="newPassword"
						placeholder="Введiть новий пароль"
						errorMessage={errors.newPassword?.message}
					/>
				</div>
				<div className="flex items-center">
					<Label className="w-[200px]" htmlFor="repeatPassword">
						Повторіть пароль
					</Label>
					<ControlledInput
						type="password"
						name="repeatPassword"
						placeholder="Введiть повторно пароль"
						errorMessage={errors.repeatPassword?.message}
					/>
				</div>
				<div className="flex items-center justify-center gap-x-5">
					<Button disabled={isDisabled} className="h-10" type="submit">
						Змінити
					</Button>
				</div>
			</Form>
		</div>
	);
};
