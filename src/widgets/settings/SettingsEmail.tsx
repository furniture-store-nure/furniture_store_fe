import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";

import {
	updateUserEmailSchema,
	type UpdateUserEmailArgs,
} from "~/server/core/validations";
import { useAuth, useMessage } from "~/hooks";
import { api } from "~/utils/api";
import { Button, ControlledInput, Form, Label } from "~/components/UI";

export const SettingsEmail = () => {
	const { logout } = useAuth();
	const m = useMessage();
	const form = useForm<UpdateUserEmailArgs>({
		mode: "onChange",
		resolver: zodResolver(updateUserEmailSchema),
	});

	const { errors } = form.formState;

	const { mutate: handleChangeEmail } = api.auth.changeEmail.useMutation({
		onSuccess: () => {
			m.success("Пошта оновлена");
			logout();
		},
		onError: (error) => {
			m.error(error.message);
		},
	});

	const handleSubmit = (data: UpdateUserEmailArgs) => {
		handleChangeEmail({
			...data,
		});
	};

	const isDisabled = !form.formState.isValid;

	return (
		<div className="w-[700px] rounded-2xl bg-white shadow-card">
			<span className="flex items-center gap-x-5 py-4 pl-6">
				<h2 className="text-xl font-semibold text-gray">Змiна пошти</h2>
			</span>
			<div className="border-b-4 border-grayMedium" />
			<Form<UpdateUserEmailArgs>
				form={form}
				onSubmit={handleSubmit}
				className="flex flex-col gap-y-5 p-6"
			>
				<div className="flex items-center">
					<Label className="w-[200px]" htmlFor="newEmail">
						Нова пошта
					</Label>
					<ControlledInput
						name="newEmail"
						placeholder="Введить нову пошту"
						errorMessage={errors.newEmail?.message}
					/>
				</div>
				<div className="flex items-center">
					<Label className="w-[200px]" htmlFor="password">
						Пароль
					</Label>
					<ControlledInput
						type="password"
						name="password"
						placeholder="Введiть пароль"
						errorMessage={errors.password?.message}
					/>
				</div>
				<div className="flex items-center justify-center gap-x-5">
					<Button disabled={isDisabled} className="h-10" type="submit">
						Змінити
					</Button>
				</div>
			</Form>
		</div>
	);
};
