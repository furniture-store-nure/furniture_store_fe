import Image from "next/image";
import {
	type Manufacturer,
	type Product,
	type Comparison,
} from "@prisma/client";

import {
	MATERIAL_OPTIONS,
	PRODUCT_TYPE_OPTIONS,
	SECURITY_OPTIONS,
} from "~/utils/constants";
import { Button } from "~/components/UI";
import { ComparisonRow } from "./ComparisonRow";

type ProductWithManufacturer = Product & {
	manufacturer: Manufacturer;
};
type ComparisonWithProduct = Comparison & {
	product: ProductWithManufacturer;
};

type ComparisonTableProps = {
	comparison1: ComparisonWithProduct;
	comparison2: ComparisonWithProduct;
	handleDeleteComparisons: () => void;
};

export const ComparisonTable = (props: ComparisonTableProps) => {
	const { comparison1, comparison2, handleDeleteComparisons } = props;

	const productTypeComparison1 = PRODUCT_TYPE_OPTIONS.find(
		(option) => option.value === comparison1.product.type,
	)!;
	const productTypeComparison2 = PRODUCT_TYPE_OPTIONS.find(
		(option) => option.value === comparison2.product.type,
	)!;

	const materialComparison1 = MATERIAL_OPTIONS.find(
		(option) => option.value === comparison1.product.material,
	)!;
	const materialComparison2 = MATERIAL_OPTIONS.find(
		(option) => option.value === comparison2.product.material,
	)!;

	const securityComparison1 = SECURITY_OPTIONS.find(
		(option) => option.value === comparison1.product.security,
	)!;
	const securityComparison2 = SECURITY_OPTIONS.find(
		(option) => option.value === comparison2.product.security,
	)!;

	return (
		<table className="relative mx-auto w-fit rounded-2xl bg-white shadow-card">
			<thead>
				<tr>
					<th className="w-[170px] rounded-tl-2xl bg-bgLight">
						<Button
							className="mx-auto w-[130px] h-[32px] !bg-red text-white"
							onClick={handleDeleteComparisons}
						>
							Видалити
						</Button>
					</th>
					<th className="w-[370px]">
						<Image
							className="h-[210px] w-[370px]"
							src={comparison1.product.logo}
							alt={comparison1.product.name}
							width={370}
							height={210}
						/>
					</th>
					<th className="w-[370px]">
						<Image
							className="h-[210px] w-[370px] rounded-tr-2xl"
							src={comparison2.product.logo}
							alt={comparison2.product.name}
							width={370}
							height={210}
						/>
					</th>
				</tr>
			</thead>
			<tbody>
				<ComparisonRow
					className="text-xl font-semibold text-gray"
					label=""
					value1={comparison1.product.name}
					value2={comparison2.product.name}
				/>
				<ComparisonRow
					className="text-xl font-semibold text-red"
					label="Цiна"
					value1={`${comparison1.product.price.toLocaleString("ru-RU")} ₴`}
					value2={`${comparison2.product.price.toLocaleString("ru-RU")} ₴`}
					highlightCondition={
						comparison1.product.price !== comparison2.product.price
					}
				/>
				<ComparisonRow
					className="text-base text-gray"
					label="Тип"
					value1={productTypeComparison1.label}
					value2={productTypeComparison2.label}
					highlightCondition={
						comparison1.product.type !== comparison2.product.type
					}
				/>
				<ComparisonRow
					className="text-base text-gray"
					label="Матерiал"
					value1={materialComparison1.label}
					value2={materialComparison2.label}
					highlightCondition={
						comparison1.product.material !== comparison2.product.material
					}
				/>
				<ComparisonRow
					className="text-base text-gray"
					label="Безпека"
					value1={securityComparison1.label}
					value2={securityComparison2.label}
					highlightCondition={
						comparison1.product.security !== comparison2.product.security
					}
				/>
				<ComparisonRow
					className="text-base text-gray"
					label="Розмiр"
					value1={`${comparison1.product.width}x${comparison1.product.height}`}
					value2={`${comparison2.product.width}x${comparison2.product.height}`}
					highlightCondition={
						comparison1.product.width !== comparison2.product.width &&
						comparison1.product.height !== comparison2.product.height
					}
				/>
				<ComparisonRow
					className="text-base text-gray"
					label="Вага"
					value1={`${comparison1.product.weight}кг`}
					value2={`${comparison2.product.weight}кг`}
					highlightCondition={
						comparison1.product.weight !== comparison2.product.weight
					}
				/>
				<ComparisonRow
					isLastRow
					classNameRow="!border-0"
					className="text-base text-gray"
					label="Виробник"
					value1={comparison1.product.manufacturer.name}
					value2={comparison2.product.manufacturer.name}
					highlightCondition={
						comparison1.product.manufacturer.id !==
						comparison2.product.manufacturer.id
					}
				/>
			</tbody>
		</table>
	);
};
