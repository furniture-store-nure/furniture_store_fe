import clsx from "classnames";

type ComparisonRowProps = {
	label: string;
	value1: string;
	value2: string;
	highlightCondition?: boolean;
	className?: string;
	classNameRow?: string;
	isLastRow?: boolean;
};

export const ComparisonRow = (props: ComparisonRowProps) => {
	const {
		classNameRow,
		className,
		label,
		value1,
		value2,
		highlightCondition = false,
		isLastRow = false,
	} = props;

	return (
		<tr
			className={clsx(
				"border-b border-beige text-center",
				{
					"bg-greenLight": highlightCondition,
				},
				classNameRow,
			)}
		>
			<td
				className={clsx("bg-bgLight text-sm font-semibold text-grayMedium2", {
					"rounded-bl-2xl": isLastRow,
					"bg-greenLight": highlightCondition,
				})}
			>
				{label}
			</td>
			<td
				className={clsx(
					"border-r border-beige py-3 text-base text-gray",
					className,
				)}
			>
				{value1}
			</td>
			<td
				className={clsx(
					"text-base text-gray",
					{
						"rounded-br-2xl": isLastRow,
					},
					className,
				)}
			>
				{value2}
			</td>
		</tr>
	);
};
