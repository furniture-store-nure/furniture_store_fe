import { useEffect } from "react";
import { useForm } from "react-hook-form";
import { useRouter } from "next/navigation";
import { type z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";

import { RoutesConfig } from "~/config/routesConfig";
import { useMessage } from "~/hooks";
import { api } from "~/utils/api";
import { signUpSchema } from "~/server/core/validations";
import { Button, ControlledInput, Form, Label, Modal } from "~/components/UI";

type FormValues = z.input<typeof signUpSchema>;

export const SignUp = () => {
	const m = useMessage();
	const router = useRouter();

	const form = useForm<FormValues>({
		mode: "onChange",
		resolver: zodResolver(signUpSchema),
	});

	const {
		formState: { errors, isValid },
	} = form;

	const { mutate: handleSignUp } = api.auth.signUp.useMutation({
		onSuccess: () => {
			router.push(RoutesConfig.Login);
			m.success("Аккаунт успішно створено");
		},
		onError: (error) => {
			m.error(error.message);
		},
	});

	const handleLogin = () => {
		router.push(RoutesConfig.Login);
	};

	const onSubmit = (data: FormValues) => {
		handleSignUp(data);
	};

	useEffect(() => {
		const values = Object.values(errors);

		if (
			values.length === 1 &&
			values[0]?.message === "Паролі повинні збігатися"
		) {
			m.error("Паролі повинні збігатися");
		}
	}, [errors, m]);

	return (
		<Modal
			title="Реєстрація"
			subTitle="Щоб зареєструватися як користувач, заповніть поля нижче"
		>
			<Form
				form={form}
				className="flex w-full flex-col items-center gap-y-5"
				onSubmit={onSubmit}
			>
				<div className="flex w-full items-center justify-between">
					<Label htmlFor="lastName">Прізвище</Label>
					<ControlledInput
						name="lastName"
						placeholder="Твоє прізвище"
						errorMessage={errors.lastName?.message}
					/>
				</div>
				<div className="flex w-full items-center justify-between">
					<Label htmlFor="firstName">Ім’я</Label>
					<ControlledInput
						name="firstName"
						placeholder="Твоє ім’я"
						errorMessage={errors.firstName?.message}
					/>
				</div>
				<div className="flex w-full items-center justify-between">
					<Label htmlFor="middleName">По-батькові</Label>
					<ControlledInput
						name="middleName"
						placeholder="Твоє по-батькові"
						errorMessage={errors.middleName?.message}
					/>
				</div>
				<div className="flex w-full items-center justify-between">
					<Label htmlFor="email">Ел. пошта</Label>
					<ControlledInput
						name="email"
						placeholder="Твоя пошта"
						errorMessage={errors.email?.message}
					/>
				</div>
				<div className="flex w-full items-center justify-between">
					<Label htmlFor="password">Пароль</Label>
					<ControlledInput
						name="password"
						type="password"
						placeholder="Твій пароль"
						errorMessage={errors.password?.message}
					/>
				</div>
				<div className="flex w-full items-center justify-between">
					<Label htmlFor="repeatPassword">Повторіть пароль</Label>
					<ControlledInput
						name="repeatPassword"
						type="password"
						placeholder="Повторіть пароль"
						// errorMessage={errors.root?.message}
					/>
				</div>
				<Button disabled={!isValid} type="submit">
					Зареєструватися
				</Button>
			</Form>
			<span className="flex">
				<p className="text-sm font-semibold tracking-wide text-turquoise">
					У вас вже є аккаунт?
				</p>
				<a
					className="cursor-pointer text-sm tracking-wide text-grayMedium2 hover:underline"
					onClick={handleLogin}
				>
					Авторизацiя
				</a>
			</span>
		</Modal>
	);
};
