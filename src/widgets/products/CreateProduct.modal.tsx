import { useState } from "react";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { Material, Security, TypeProduct } from "@prisma/client";
import { z } from "zod";

import { api } from "~/utils/api";
import { setOpenModal } from "~/redux/app";
import { useAppDispatch } from "~/redux/hooks";
import { useMessage } from "~/hooks";
import { useGetManufacturers } from "~/hooks/products";
import {
	MATERIAL_OPTIONS,
	PRODUCT_TYPE_OPTIONS,
	SECURITY_OPTIONS,
} from "~/utils/constants";
import { FileToBase64 } from "~/utils/helpers";
import {
	AttachButton,
	Button,
	ControlledInput,
	ControlledSelect,
	Form,
	Label,
	Modal,
} from "~/components/UI";

type CreateProductModalProps = {
	handleRefetchProducts: () => void;
};

const selectOptionManufacturerSchema = z.object({
	value: z.string(),
	label: z.string(),
});

const selectOptionTypeSchema = z.object({
	value: z.nativeEnum(TypeProduct),
	label: z.string(),
});

const selectOptionMaterialSchema = z.object({
	value: z.nativeEnum(Material),
	label: z.string(),
});

const selectOptionSecuritySchema = z.object({
	value: z.nativeEnum(Security),
	label: z.string(),
});

export const createProductSchema = z.object({
	name: z
		.string()
		.regex(
			new RegExp(/^[a-zA-Zа-щА-ЩіІїЇєЄґҐ' ]+$/),
			"Назва повинна містити лише літери",
		),
	type: selectOptionTypeSchema,
	material: selectOptionMaterialSchema,
	security: selectOptionSecuritySchema,
	manufacturer: selectOptionManufacturerSchema,
	width: z.string().refine((val) => Number(val) >= 0, {
		message: "Ширина повинна бути додатнім числом",
	}),
	height: z.string().refine((val) => Number(val) >= 0, {
		message: "Висота повинна бути додатнім числом",
	}),
	weight: z.string().refine((val) => Number(val) >= 0, {
		message: "Вага повинна бути додатнім числом",
	}),
	price: z.string().refine((val) => Number(val) >= 0, {
		message: "Ціна повинна бути додатнім числом",
	}),
	logo: z.instanceof(File),
});

export type CreateProductArgs = z.input<typeof createProductSchema>;

export const CreateProductModal = (props: CreateProductModalProps) => {
	const { handleRefetchProducts } = props;

	const m = useMessage();
	const dispatch = useAppDispatch();

	const [logo, setLogo] = useState<File | undefined>();

	const form = useForm<CreateProductArgs>({
		mode: "onChange",
		resolver: zodResolver(createProductSchema),
	});

	const {
		formState: { isValid, errors },
	} = form;

	const { manufacturers } = useGetManufacturers();

	const { mutate: handleCreateProduct } = api.product.createProduct.useMutation(
		{
			onSuccess: () => {
				m.success("Вироб успішно створено");
				handleClose();
				handleRefetchProducts();
			},
			onError: (error) => {
				m.error(error.message);
			},
		},
	);

	const handleClose = () => {
		dispatch(setOpenModal(null));
	};

	const handleAttachLogo = (file: File) => {
		form.setValue("logo", file, { shouldValidate: true });
		setLogo(file);
	};

	const handleDeleteLogo = () => {
		form.resetField("logo");
		setLogo(undefined);
	};

	const handleSubmit = async (data: CreateProductArgs) => {
		const base64 = (await FileToBase64(data.logo)) as string;

		const creationData = {
			...data,
			manufacturerId: data.manufacturer.value,
			material: data.material.value,
			security: data.security.value,
			type: data.type.value,
			width: data.width,
			height: data.height,
			weight: data.weight,
			price: Number(data.price),
			logo: base64,
		};

		handleCreateProduct(creationData);
	};

	const isDisabledSubmit = !isValid || !logo;

	return (
		<Modal
			className="h-[970px] !gap-y-14"
			withBackground
			title="Створення меблевого виробу"
			handleClose={handleClose}
		>
			<Form<CreateProductArgs>
				className="flex size-full flex-col items-center justify-between"
				form={form}
				onSubmit={handleSubmit}
			>
				<div className="flex w-full flex-col gap-y-5">
					<div className="flex items-center justify-between">
						<Label htmlFor="name">Назва</Label>
						<ControlledInput
							name="name"
							placeholder="Введiть назву виробу"
							type="text"
							errorMessage={errors.name?.message}
						/>
					</div>
					<div className="flex items-center justify-between">
						<Label htmlFor="type">Вид</Label>
						<ControlledSelect
							name="type"
							className="h-14 w-[392px]"
							placeholder="Виберiть вид виробу"
							options={PRODUCT_TYPE_OPTIONS}
						/>
					</div>
					<div className="flex items-center justify-between">
						<Label htmlFor="material">Матерiал</Label>
						<ControlledSelect
							name="material"
							className="h-14 w-[392px]"
							placeholder="Виберiть матерiал"
							options={MATERIAL_OPTIONS}
						/>
					</div>
					<div className="flex items-center justify-between">
						<Label htmlFor="security">Безпека</Label>
						<ControlledSelect
							name="security"
							className="h-14 w-[392px]"
							placeholder="Виберiть безпеку"
							options={SECURITY_OPTIONS}
						/>
					</div>
					<div className="flex items-center justify-between">
						<Label htmlFor="manufacturer">Виробник</Label>
						<ControlledSelect
							name="manufacturer"
							className="h-14 w-[392px]"
							placeholder="Виберiть виробника"
							options={manufacturers}
						/>
					</div>
					<div className="flex items-center justify-between">
						<Label htmlFor="width">Ширина</Label>
						<ControlledInput
							name="width"
							placeholder="Введiть ширину"
							type="number"
							errorMessage={errors.width?.message}
						/>
					</div>
					<div className="flex items-center justify-between">
						<Label htmlFor="height">Висота</Label>
						<ControlledInput
							name="height"
							placeholder="Введiть висоту"
							type="number"
							errorMessage={errors.height?.message}
						/>
					</div>
					<div className="flex items-center justify-between">
						<Label htmlFor="weight">Вага (кг)</Label>
						<ControlledInput
							className="w-[392px]"
							name="weight"
							placeholder="Введiть вагу"
							type="number"
							errorMessage={errors.weight?.message}
						/>
					</div>
					<div className="flex items-center justify-between">
						<Label htmlFor="price">Ціна</Label>
						<ControlledInput
							name="price"
							placeholder="Введiть цiну"
							type="number"
							errorMessage={errors.price?.message}
						/>
					</div>
					<div className="flex min-h-14 items-center justify-between">
						<Label htmlFor="logo">Зображення</Label>
						<AttachButton
							className="flex w-[392px] items-center"
							file={logo}
							handleAttach={handleAttachLogo}
							handleDelete={handleDeleteLogo}
						/>
					</div>
				</div>
				<div className="flex items-center gap-x-5">
					<Button
						type="submit"
						disabled={isDisabledSubmit}
						className="w-[200px]"
						variant="primary"
					>
						Створити
					</Button>
					<Button
						type="reset"
						className="w-[200px]"
						variant="secondary"
						onClick={handleClose}
					>
						Скасувати
					</Button>
				</div>
			</Form>
		</Modal>
	);
};
