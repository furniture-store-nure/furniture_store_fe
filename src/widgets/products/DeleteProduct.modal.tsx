import { Button, Modal } from "~/components/UI";
import { useMessage } from "~/hooks";
import { setOpenModal } from "~/redux/app";
import { useAppDispatch } from "~/redux/hooks";
import { api } from "~/utils/api";

type DeleteProductProps = {
	productId: string;
	handleRefetchProducts: () => void;
};

export const DeleteProductModal = (props: DeleteProductProps) => {
	const { productId, handleRefetchProducts } = props;

	const m = useMessage();
	const dispatch = useAppDispatch();

	const { mutate: handleDeleteProduct } = api.product.deleteProduct.useMutation(
		{
			onSuccess: () => {
				m.success("Вироб успішно видалено");
				handleClose();
				handleRefetchProducts();
			},
			onError: (error) => {
				handleClose();
				m.error(error.message);
			},
		},
	);

	const handleClose = () => {
		dispatch(setOpenModal(null));
	};

	const handleSubmit = () => {
		handleDeleteProduct({ productId });
	};

	return (
		<>
			<Modal
				className="h-[250px] !gap-y-10"
				withBackground
				title="Видалення меблевого виробу"
				subTitle="Ви впевнені, що хочете видалити меблевий виріб?"
				handleClose={handleClose}
			>
				<div className="flex gap-x-5">
					<Button
						className="w-[200px] !bg-red !text-white"
						onClick={handleSubmit}
					>
						Видалити
					</Button>
					<Button
						className="w-[200px]"
						variant="secondary"
						onClick={handleClose}
					>
						Скасувати
					</Button>
				</div>
			</Modal>
		</>
	);
};
