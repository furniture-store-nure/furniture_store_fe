/* eslint-disable @typescript-eslint/no-explicit-any */

type DeepNonNullable<T> = {
	[P in keyof T]-?: NonNullable<T[P]>;
} & NonNullable<T>;

type AsyncReturnType<T extends (...args: any) => Promise<any>> = T extends (
	...args: any
) => Promise<infer R>
	? R
	: any;
