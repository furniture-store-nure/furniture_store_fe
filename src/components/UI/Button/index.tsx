import clsx from "classnames";

type ButtonProps = {
	children: React.ReactNode;
	variant?: "primary" | "secondary";
} & React.ButtonHTMLAttributes<HTMLButtonElement>;

export const Button = (props: ButtonProps) => {
	const {
		disabled,
		children,
		variant = "primary",
		className = "w-fit",
		...rest
	} = props;

	return (
		<button
			className={clsx(
				"flex items-center justify-center rounded-md px-[30px] py-[15px] font-openSans text-base font-semibold uppercase tracking-wider text-gray",
				{
					"cursor-not-allowed opacity-75": disabled,
					"bg-turquoise": variant === "primary",
					"border-2 border-turquoise": variant === "secondary",
				},
				className,
			)}
			{...rest}
		>
			{children}
		</button>
	);
};
