import clsx from "classnames";

type LabelProps = {
	children: React.ReactNode;
} & React.LabelHTMLAttributes<HTMLLabelElement>;

export const Label = (props: LabelProps) => {
	const { children } = props;

	return (
		<label
			className={clsx("text-base tracking-wide text-grayDark", props.className)}
		>
			{children}
		</label>
	);
};
