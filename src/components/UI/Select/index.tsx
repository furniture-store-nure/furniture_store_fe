import { Fragment, forwardRef } from "react";
import { Listbox, Transition } from "@headlessui/react";
import clsx from "classnames";
import { useController, useFormContext } from "react-hook-form";

import { Icon, type IconProp } from "~/components/UI";

export type SelectOption<T> = {
	label: string | null;
	value: T;
};

type SelectProps<T> = {
	variant?: "default" | "outlined";
	startIcon?: IconProp;
	placeholder?: string;
	options: SelectOption<T>[];
	value: SelectOption<T> | null;
	onChange?: (value: SelectOption<T>) => void;
	fullWidth?: boolean;
	className?: string;
	arrowIcon?: IconProp;
	dropDownClassName?: string;
	listItemClassName?: string;
	iconClassName?: string;
	defaultValue?: SelectOption<T>;
	selectedTextClassName?: string;
	disabled?: boolean;
};

const withSelect = <T,>() =>
	forwardRef<HTMLDivElement, SelectProps<T>>((props, ref) => {
		const {
			variant = "default",
			startIcon,
			options,
			value,
			placeholder = "Select",
			fullWidth,
			onChange,
			className = "h-14 w-[313px]",
			arrowIcon = "down_arrow_green",
			dropDownClassName = "top-16 z-30",
			listItemClassName,
			iconClassName,
			selectedTextClassName,
			disabled,
		} = props;

		return (
			<Listbox
				value={value}
				onChange={onChange}
				disabled={disabled || options.length === 0}
			>
				{({ open }) => (
					<div
						ref={ref}
						className={clsx("relative flex flex-col", {
							"h-14": variant === "default",
							"h-fit": variant === "outlined",
						})}
					>
						<Listbox.Button
							className={clsx(
								"flex items-center justify-between gap-x-2 rounded-lg bg-white pb-3 pl-4 pr-6 pt-[18px]",
								{
									"h-14": variant === "default",
									"h-fit": variant === "outlined",
									"w-full": fullWidth,
									"cursor-default": disabled,
									"border border-turquoise": open && variant === "default",
									"border border-grayMedium":
										!disabled && variant === "default",
								},
								className,
							)}
						>
							{startIcon && <Icon className="shrink-0" icon={startIcon} />}
							<div
								className={clsx("flex w-full items-center gap-x-2", {
									"h-14": variant === "default",
									"h-fit": variant === "outlined",
								})}
							>
								<span
									className={clsx(
										"text-base",
										{
											"text-grayMedium": !value?.label && variant === "default",
											"text-gray": value?.label,
										},
										selectedTextClassName,
									)}
								>
									{value?.label ?? placeholder}
								</span>
							</div>
							<Icon
								className={clsx(
									"z-20 h-[10px] w-[20px] shrink-0",
									{
										"rotate-180 transition delay-150 ease-in-out":
											open && !disabled,
										"rotate-0 transition delay-150 ease-in-out":
											!open && !disabled,
									},
									iconClassName,
								)}
								icon={arrowIcon}
							/>
						</Listbox.Button>

						{!disabled && (
							<Transition
								show={open}
								as={Fragment}
								leave="transition ease-in duration-100"
								leaveFrom="opacity-100"
								leaveTo="opacity-0"
							>
								<Listbox.Options
									style={{
										scrollbarWidth: "none",
									}}
									className={clsx(
										"absolute z-10 max-h-60 w-full overflow-auto rounded-2xl border-2 border-turquoise bg-white text-base tracking-wider text-gray shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none",
										dropDownClassName,
									)}
								>
									{options.map((option, idx) => (
										<Listbox.Option
											key={idx}
											className={() =>
												clsx(
													"relative cursor-pointer select-none pl-4 pt-4 hover:font-poppins hover:font-semibold",
													{
														"pb-4": idx === options.length - 1,
													},
													listItemClassName,
												)
											}
											value={option}
										>
											<div className="flex items-center">
												<span className="block truncate">{option.label}</span>
											</div>
										</Listbox.Option>
									))}
								</Listbox.Options>
							</Transition>
						)}
					</div>
				)}
			</Listbox>
		);
	});

export const Select = <T,>(props: SelectProps<T>) => {
	const Component = withSelect<T>();
	return <Component {...props} />;
};

Select.displayName = "Select";

type ControlledSelectProps<T> = Omit<SelectProps<T>, "value"> & {
	name: string;
	disabled?: boolean;
	value?: SelectOption<T> | undefined;
};

export const ControlledSelect = <T,>(props: ControlledSelectProps<T>) => {
	const { name, onChange, defaultValue, disabled, ...rest } = props;

	const { control } = useFormContext();
	const { field } = useController({
		name,
		control,
		defaultValue: defaultValue ?? null,
	});

	const { onChange: controlOnChange, ...restField } = field;

	const handleOnChange = (value: SelectOption<T>) => {
		if (disabled) return;
		controlOnChange(value);
		typeof onChange === "function" && onChange(value);
	};

	return (
		<Select
			{...restField}
			{...rest}
			disabled={disabled}
			value={field.value}
			onChange={handleOnChange}
		/>
	);
};

ControlledSelect.displayName = "ControlledSelect";
