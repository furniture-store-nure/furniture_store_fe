import { useCallback, useState } from "react";
import { useRouter } from "next/router";
import clsx from "classnames";
import { Icon } from "../Icon";

export type TableColumn<TableDataTypes> = {
	title: string;
	dataIndex: keyof TableDataTypes;
	thClassName?: string;
	tdClassName?: string;
	render?: (value: keyof TableDataTypes, record: TableDataTypes) => JSX.Element;
};

type TableProps<TableDataTypes> = {
	columns: TableColumn<TableDataTypes>[];
	rows: TableDataTypes[];
	rowName: string;
	totalCount: number;
	limit: number;
	withVerticalLines?: boolean;
	onChangePage?: (page: number) => void;
};

export const Table = <T extends Record<string, any>>(props: TableProps<T>) => {
	const {
		rows,
		columns,
		totalCount,
		withVerticalLines = true,
		rowName,
		limit,
		onChangePage,
	} = props;

	const router = useRouter();

	const [page, setPage] = useState(
		router.query.page ? Number(router.query.page) : 1,
	);

	const handlePageChange = useCallback(
		(page: number) => {
			setPage(page);
			if (onChangePage) {
				onChangePage(page);
			}
		},
		[setPage, onChangePage],
	);

	const countOfAllPages = Math.ceil(totalCount / limit);

	return (
		<div className="rounded-2xl bg-white shadow-card">
			<table className="w-full">
				<thead className="grayMedium2 h-[4rem] bg-bgLight text-sm font-semibold">
					<tr>
						{columns.map((column, index) => (
							<th
								key={`header_${index}`}
								className={clsx(
									"text-sm font-semibold tracking-wider text-grayMedium2",
									{
										"border-l border-beige": index !== 0 && withVerticalLines,
										"rounded-tl-2xl": index === 0,
										"rounded-tr-2xl": index === columns.length - 1,
									},
									column.thClassName,
								)}
							>
								{column.title}
							</th>
						))}
					</tr>
				</thead>
				<tbody>
					{rows.map((record) => (
						<tr key={record.id} className="group table-row">
							{columns.map(({ dataIndex, render, tdClassName }, colIdx) => (
								<td
									key={colIdx}
									className={clsx(
										"table-cell h-[8.75rem] border-b border-beige px-2 pt-5 align-top",
										{
											"border-l border-beige":
												colIdx !== 0 && withVerticalLines,
										},
										tdClassName,
									)}
								>
									{render ? render(dataIndex, record) : record[dataIndex]}
								</td>
							))}
						</tr>
					))}
				</tbody>
			</table>
			<div className="flex items-center justify-between">
				<p className="flex h-11 items-center pl-5 text-sm font-light tracking-wide text-gray">
					{rowName} {rows.length} / {totalCount}
				</p>
				<div className="pr-5">
					<PaginationRange
						handlePageChange={handlePageChange}
						currentPage={page}
						countOfAllPages={countOfAllPages}
					/>
				</div>
			</div>
		</div>
	);
};

type PaginationRangeProps = {
	handlePageChange: (page: number) => void;
	currentPage: number;
	countOfAllPages: number;
};

const PaginationRange = (props: PaginationRangeProps) => {
	const { handlePageChange, currentPage, countOfAllPages } = props;

	const isFirstPage = currentPage === 0 || currentPage === 1;

	const isLastPage = currentPage >= countOfAllPages;

	const setPrevPage = () => {
		if (isFirstPage) return;
		handlePageChange(currentPage - 1);
	};

	const setNextPage = () => {
		if (isLastPage) return;
		handlePageChange(currentPage + 1);
	};

	const handleSetPage = (page: number) => {
		handlePageChange(page);
	};

	const createPageData = Array.from(
		{ length: countOfAllPages - 1 },
		(_, i) => i + 1,
	);

	const activeRange = [...createPageData].slice(
		currentPage > 2 ? currentPage - 1 : currentPage,
		currentPage + 2,
	);

	const isItemInArr = (item: number) =>
		item === currentPage - 1 ||
		item === currentPage ||
		item === currentPage + 1;

	return (
		<div className="flex gap-x-[15px]">
			<button onClick={setPrevPage}>
				<Icon
					icon="left_arrow"
					className={clsx({
						"rounded-sm hover:bg-beige hover:text-gray": !isFirstPage,
						"text-grayMedium": isFirstPage,
					})}
				/>
			</button>
			<div className="flex h-[24px] gap-x-[15px] text-sm">
				{!activeRange.some((i) => i === 0) && (
					<button
						className={clsx({
							"size-6 rounded-sm bg-beige font-poppins font-semibold text-gray":
								currentPage === 1,
							"font-light": currentPage !== 1,
						})}
						onClick={() => handleSetPage(1)}
					>
						1
					</button>
				)}
				{currentPage > 3 && <div>...</div>}
				{createPageData.map((item, index) => {
					return (
						isItemInArr(item + 1) && (
							<button
								key={`${index}_pagination`}
								onClick={() => handleSetPage(item + 1)}
								className={clsx({
									"size-6 rounded-sm bg-beige font-poppins text-sm font-semibold text-gray":
										item + 1 === currentPage,
									"font-light": currentPage !== item + 1,
								})}
							>
								{item + 1}
							</button>
						)
					);
				})}
				{currentPage < countOfAllPages - 2 && <div>...</div>}
				{currentPage < countOfAllPages - 1 && (
					<button
						className={clsx({
							"size-6 rounded-sm bg-beige font-poppins text-sm font-semibold text-gray":
								countOfAllPages === currentPage,
							"font-light": currentPage !== countOfAllPages,
						})}
						onClick={() => handleSetPage(countOfAllPages)}
					>
						{countOfAllPages}
					</button>
				)}
			</div>
			<button onClick={setNextPage}>
				<Icon
					icon="right_arrow"
					className={clsx({
						"rounded-sm hover:bg-beige hover:text-gray": !isLastPage,
						"text-grayMedium": isLastPage,
					})}
				/>
			</button>
		</div>
	);
};
