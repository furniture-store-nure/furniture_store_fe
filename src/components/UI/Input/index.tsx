import React from "react";
import { useController, useFormContext } from "react-hook-form";
import clsx from "classnames";
import { Icon } from "../Icon";

type InputProps = {
	variant?: "numeric" | "default";
	errorMessage?: string;
	classNameError?: string;
} & React.InputHTMLAttributes<HTMLInputElement>;

export const Input = React.forwardRef<HTMLDivElement, InputProps>(
	(props, ref) => {
		const {
			className = "w-[392px]",
			errorMessage,
			disabled = false,
			variant = "default",
			classNameError = "w-[392px]",
			...rest
		} = props;

		const handleIncrement = () => {
			if (!disabled && rest.onChange) {
				const newValue = (Number(rest.value) + 1).toString();
				const event = {
					target: { value: newValue },
					currentTarget: { value: newValue },
					preventDefault: () => {},
					stopPropagation: () => {},
				} as React.ChangeEvent<HTMLInputElement>;
				rest.onChange(event);
			}
		};

		const handleDecrement = () => {
			if (!disabled && rest.onChange) {
				if (Number(rest.value) - 1 < 1) return;

				const newValue = (Number(rest.value) - 1).toString();
				const event = {
					target: { value: newValue },
					currentTarget: { value: newValue },
					preventDefault: () => {},
					stopPropagation: () => {},
				} as React.ChangeEvent<HTMLInputElement>;
				rest.onChange(event);
			}
		};

		const handleKeyUp = (el: React.KeyboardEvent<HTMLInputElement>) => {
			if (el.currentTarget.value != "") {
				if (parseInt(el.currentTarget.value) < parseInt(el.currentTarget.min)) {
					el.currentTarget.value = el.currentTarget.min;
				}
				if (parseInt(el.currentTarget.value) > parseInt(el.currentTarget.max)) {
					el.currentTarget.value = el.currentTarget.max;
				}
			}
		};

		return (
			<div ref={ref} className="relative flex w-fit flex-col">
				<input
					disabled={disabled}
					onKeyUp={variant === "numeric" ? handleKeyUp : undefined}
					className={clsx(
						"h-14 rounded-lg pb-3 pl-4 pr-[45px] pt-3 text-base tracking-wide placeholder:tracking-wide focus:outline-none",
						{
							"border border-grayMedium bg-white text-gray placeholder:text-grayMedium":
								!errorMessage && !disabled,
							"border-2 border-red bg-white text-gray placeholder:text-grayMedium":
								errorMessage && !disabled,
						},
						className,
					)}
					{...rest}
				/>
				{variant === "numeric" && (
					<div className="absolute right-4 top-3 flex flex-col gap-y-3">
						<button type="button" onClick={handleIncrement}>
							<Icon icon="down_arrow_green" className="rotate-180" />
						</button>
						<button type="button" onClick={handleDecrement}>
							<Icon icon="down_arrow_green" />
						</button>
					</div>
				)}
				{errorMessage && (
					<p
						className={clsx(
							"left-0 top-14 z-10 break-all pl-4 text-xs text-red",
							classNameError,
						)}
					>
						{errorMessage}
					</p>
				)}
			</div>
		);
	},
);

Input.displayName = "Input";

type ControlledInputProps = InputProps & {
	name: string;
	handleChange?: (data: string) => void;
	defaultValue?: string | undefined;
	max?: number;
	valueAsNumber?: boolean;
	disabled?: boolean;
};

export const ControlledInput = (props: ControlledInputProps) => {
	const { name, handleChange, defaultValue, max, disabled, ...rest } = props;

	const { control } = useFormContext();
	const { field } = useController({
		name,
		control,
		defaultValue: defaultValue ?? "",
	});

	const {
		onChange: internalOnChange,
		onBlur: controlOnBlur,
		...restField
	} = field;

	// useEffect(() => {
	// 	const timer = setTimeout(() => {
	// 		setValue(name, externalValue ?? "");
	// 	}, 0);

	// 	return () => clearTimeout(timer);
	// }, [setValue, externalValue, name]);

	const handleOnChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		if (disabled) return;
		const { value } = e.target;

		if (max && value.length > max) return;

		internalOnChange(value);

		if (handleChange) {
			handleChange(value);
		}
	};

	return (
		<Input
			{...restField}
			{...rest}
			disabled={disabled}
			onChange={handleOnChange}
			onBlur={controlOnBlur}
		/>
	);
};
