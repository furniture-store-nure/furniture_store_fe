import { forwardRef, useCallback, useState } from "react";
import { useController, useFormContext } from "react-hook-form";
import clsx from "classnames";

type RadioButtonProps = {
	id?: string;
	name: string;
	label?: string;
	checked?: boolean;
	value: string;
	onValue?: (value: string) => void;
	onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
};

export const RadioButton = forwardRef<HTMLDivElement, RadioButtonProps>(
	(props, ref) => {
		const {
			id,
			name,
			label,
			checked = false,
			value,
			onValue,
			onChange,
		} = props;

		const [isHovered, setIsHovered] = useState(false);

		const handleChange = useCallback(
			(e: React.ChangeEvent<HTMLInputElement>) => {
				typeof onValue === "function" && onValue(e.currentTarget.value);
			},
			[onValue],
		);

		return (
			<div
				className="flex items-center"
				ref={ref}
				onMouseEnter={() => setIsHovered(true)}
				onMouseLeave={() => setIsHovered(false)}
				onClick={() =>
					handleChange({
						currentTarget: { value },
					} as React.ChangeEvent<HTMLInputElement>)
				}
			>
				<div className="relative flex h-5 items-center">
					<input
						id={id}
						type="radio"
						name={name}
						value={value}
						checked={checked}
						className="hidden size-5 rounded-sm border-2 border-grayMedium checked:border-gray checked:bg-gray hover:border-gray"
						onChange={onChange ? onChange : handleChange}
					/>
					<div className="cursor-pointer">
						{checked ? (
							<CheckedIcon />
						) : isHovered ? (
							<HoverIcon />
						) : (
							<DefaultIcon />
						)}
					</div>
				</div>
				<label
					htmlFor={id}
					className={clsx("ml-[10px] cursor-pointer", {
						"text-grayMedium2": !isHovered && !checked,
						"text-gray": isHovered || checked,
					})}
				>
					{label && (
						<span className="block text-base tracking-wider">{label}</span>
					)}
				</label>
			</div>
		);
	},
);

RadioButton.displayName = "RadioButton";

type ControlledRadioButtonProps = RadioButtonProps & {
	name: string;
	defaultValue?: string;
	handleChange?: (e: string) => void;
};

export const ControlledRadioButton = (props: ControlledRadioButtonProps) => {
	const { name, defaultValue, handleChange, ...rest } = props;

	const { control } = useFormContext();

	const { field } = useController({
		name,
		control,
		defaultValue: defaultValue ?? "",
	});

	const { onChange: controlOnChange, ...restField } = field;

	const handleOnChange = (value: string) => {
		controlOnChange(value);
		if (handleChange) handleChange(value);
	};

	return (
		<RadioButton
			{...restField}
			{...rest}
			onValue={handleOnChange}
			checked={field.value === props.value}
		/>
	);
};

ControlledRadioButton.displayName = "ControlledRadioButton";

const HoverIcon = () => {
	return (
		<svg
			width="24"
			height="24"
			viewBox="0 0 24 24"
			fill="none"
			xmlns="http://www.w3.org/2000/svg"
		>
			<path
				fillRule="evenodd"
				clipRule="evenodd"
				d="M2 12C2 6.48 6.48 2 12 2C17.52 2 22 6.48 22 12C22 17.52 17.52 22 12 22C6.48 22 2 17.52 2 12ZM4 12C4 16.42 7.58 20 12 20C16.42 20 20 16.42 20 12C20 7.58 16.42 4 12 4C7.58 4 4 7.58 4 12Z"
				fill="#454545"
			/>
		</svg>
	);
};

const CheckedIcon = () => {
	return (
		<svg
			width="24"
			height="24"
			viewBox="0 0 24 24"
			fill="none"
			xmlns="http://www.w3.org/2000/svg"
		>
			<path
				fillRule="evenodd"
				clipRule="evenodd"
				d="M2 12C2 6.48 6.48 2 12 2C17.52 2 22 6.48 22 12C22 17.52 17.52 22 12 22C6.48 22 2 17.52 2 12ZM4 12C4 16.42 7.58 20 12 20C16.42 20 20 16.42 20 12C20 7.58 16.42 4 12 4C7.58 4 4 7.58 4 12Z"
				fill="#454545"
			/>
			<path
				d="M12 17C14.7614 17 17 14.7614 17 12C17 9.23858 14.7614 7 12 7C9.23858 7 7 9.23858 7 12C7 14.7614 9.23858 17 12 17Z"
				fill="#5FC8C3"
			/>
		</svg>
	);
};

const DefaultIcon = () => {
	return (
		<svg
			width="24"
			height="24"
			viewBox="0 0 24 24"
			fill="none"
			xmlns="http://www.w3.org/2000/svg"
		>
			<path
				fillRule="evenodd"
				clipRule="evenodd"
				d="M2 12C2 6.48 6.48 2 12 2C17.52 2 22 6.48 22 12C22 17.52 17.52 22 12 22C6.48 22 2 17.52 2 12ZM4 12C4 16.42 7.58 20 12 20C16.42 20 20 16.42 20 12C20 7.58 16.42 4 12 4C7.58 4 4 7.58 4 12Z"
				fill="#BEBEBE"
			/>
		</svg>
	);
};
