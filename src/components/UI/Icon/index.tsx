import { type FunctionComponent, type SVGProps, memo } from "react";
import clsx from "classnames";

import { icons } from "~/assets/icons";

export type IconProp = keyof typeof icons;

type IconProps = {
	icon: IconProp;
	className?: string;
	onClick?: () => void;
};

export const Icon = memo((props: IconProps) => {
	const { icon, className, onClick } = props;

	const IconComponent: FunctionComponent<SVGProps<SVGElement>> = icons[icon];

	return (
		<IconComponent
			className={clsx(
				{
					"h-fit w-fit": !className,
					"cursor-pointer": onClick,
				},
				className,
			)}
			onClick={onClick}
		/>
	);
});
