import clsx from "classnames";
import { useEffect } from "react";

interface ModalProps {
	title: string;
	subTitle?: string;
	className?: string;
	children: React.ReactNode;
	withBackground?: boolean;
	handleClose?: () => void;
}

export const Modal = (props: ModalProps) => {
	const {
		title,
		subTitle,
		className,
		withBackground = false,
		children,
		handleClose,
	} = props;

	const closeHandler = () => {
		handleClose && handleClose();
	};

	useEffect(() => {
		const originalStyle = window.getComputedStyle(document.body).overflow;
		document.body.style.overflow = "hidden";
		return () => {
			document.body.style.overflow = originalStyle;
		};
	}, []);

	return (
		<>
			<div
				onClick={closeHandler}
				className={clsx("fixed inset-0 z-30 flex h-full w-screen", {
					"bg-black bg-opacity-40": withBackground,
				})}
			/>
			<div
				className="fixed z-50 flex items-center justify-center overflow-y-auto"
				style={{ top: "50%", left: "50%", transform: "translate(-50%, -50%)" }}
			>
				<div
					className={clsx(
						"flex w-[40rem] flex-col items-center gap-y-[30px] rounded-2xl border-2 border-turquoise bg-white p-[30px]",
						className,
					)}
				>
					<h2 className="text-2xl font-semibold text-grayDark">{title}</h2>
					{subTitle && (
						<h3 className="text-center text-base text-grayMedium2">
							{subTitle}
						</h3>
					)}
					{children}
				</div>
			</div>
		</>
	);
};
