import Image from "next/image";

import { Icon } from "../Icon";
import { useMessage } from "~/hooks";

type AttachProps = {
	className?: string;
	file?: File;
	handleAttach: (file: File) => void;
	handleDelete: () => void;
};

export const AttachButton = (props: AttachProps) => {
	const { className, file = null, handleAttach, handleDelete } = props;

	const m = useMessage();

	const handleAttachFile = (e: React.ChangeEvent<HTMLInputElement>) => {
		const files = e.target.files ?? [];

		const file = files[0];

		if (file && (file.type === "image/png" || file.type === "image/jpeg")) {
			handleAttach(file);
		} else {
			m.error("Будь ласка, виберіть зображення у форматі PNG або JPEG.");
		}
	};

	return (
		<div className={className}>
			{file ? (
				<div className="flex items-center gap-x-4">
					<Image
						className="size-[60px] rounded-[4px]"
						src={URL.createObjectURL(file)}
						width={60}
						height={60}
						alt={file.name}
					/>
					<Icon
						className="size-fit cursor-pointer"
						icon="trash"
						onClick={handleDelete}
					/>
				</div>
			) : (
				<>
					<label
						className="flex cursor-pointer gap-x-1 text-sm font-semibold tracking-wider text-gray"
						htmlFor="attach"
					>
						<Icon className="size-fit cursor-pointer" icon="attach_file" />
						Додати зображення
					</label>
					<input
						type="file"
						id="attach"
						style={{ display: "none" }}
						onChange={handleAttachFile}
					/>
				</>
			)}
		</div>
	);
};
