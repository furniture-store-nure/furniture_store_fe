import { forwardRef, useCallback, useState } from "react";
import { useController, useFormContext } from "react-hook-form";
import clsx from "classnames";

type CheckboxProps = {
	id?: string;
	name: string;
	label?: string;
	checked?: boolean;
	onValue?: (checked: boolean) => void;
	onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
};

export const Checkbox = forwardRef<HTMLDivElement, CheckboxProps>(
	(props, ref) => {
		const { id, name, label, checked = false, onValue } = props;

		const [isHovered, setIsHovered] = useState(false);

		const handleChange = useCallback(
			(e: React.ChangeEvent<HTMLInputElement>) => {
				typeof onValue === "function" && onValue(e.currentTarget.checked);
			},
			[onValue],
		);

		return (
			<div
				className="flex items-center"
				ref={ref}
				onMouseEnter={() => setIsHovered(true)}
				onMouseLeave={() => setIsHovered(false)}
				onClick={() =>
					handleChange({
						currentTarget: { checked: !checked },
					} as React.ChangeEvent<HTMLInputElement>)
				}
			>
				<div className="relative flex h-5 items-center">
					<input
						checked={checked}
						id={id}
						name={name}
						type="checkbox"
						className="hidden size-5 rounded-sm border-2 border-grayMedium checked:border-gray checked:bg-gray hover:border-gray"
						onChange={handleChange}
					/>
					<div className="cursor-pointer">
						{checked ? (
							<CheckedIcon />
						) : isHovered ? (
							<HoverIcon />
						) : (
							<DefaultIcon />
						)}
					</div>
				</div>
				<label
					htmlFor={id}
					className={clsx("ml-[10px] cursor-pointer", {
						"text-grayMedium2": !isHovered && !checked,
						"text-gray": isHovered || checked,
					})}
				>
					{label && (
						<span className="block text-base tracking-wider">{label}</span>
					)}
				</label>
			</div>
		);
	},
);

Checkbox.displayName = "Checkbox";

type ControlledCheckboxProps = CheckboxProps & {
	name: string;
	defaultValue?: boolean;
	handleChange?: (e: boolean) => void;
};

export const ControlledCheckbox = (props: ControlledCheckboxProps) => {
	const { name, defaultValue, handleChange, ...rest } = props;

	const { control } = useFormContext();

	const { field } = useController({
		name,
		control,
		defaultValue: defaultValue ?? false,
	});

	const { onChange: controlOnChange, ...restField } = field;

	const handleOnChange = (checked: boolean) => {
		controlOnChange(checked);
		if (handleChange) handleChange(checked);
	};

	return (
		<Checkbox
			{...restField}
			{...rest}
			onValue={handleOnChange}
			checked={field.value}
		/>
	);
};

const CheckedIcon = () => {
	return (
		<svg
			xmlns="http://www.w3.org/2000/svg"
			width="24"
			height="24"
			viewBox="0 0 24 24"
			fill="none"
		>
			<path
				d="M19 3H5C3.9 3 3 3.9 3 5V19C3 20.1 3.9 21 5 21H19C20.1 21 21 20.1 21 19V5C21 3.9 20.1 3 19 3Z"
				fill="#454545"
			/>
			<path
				d="M17 9L11 15L8 12"
				stroke="#5FC8C3"
				strokeWidth="2"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
			<path
				fillRule="evenodd"
				clipRule="evenodd"
				d="M5 3H19C20.1 3 21 3.9 21 5V19C21 20.1 20.1 21 19 21H5C3.9 21 3 20.1 3 19V5C3 3.9 3.9 3 5 3ZM6 19H18C18.55 19 19 18.55 19 18V6C19 5.45 18.55 5 18 5H6C5.45 5 5 5.45 5 6V18C5 18.55 5.45 19 6 19Z"
				fill="#454545"
			/>
		</svg>
	);
};

const HoverIcon = () => {
	return (
		<svg
			xmlns="http://www.w3.org/2000/svg"
			width="24"
			height="24"
			viewBox="0 0 24 24"
			fill="none"
		>
			<path
				fillRule="evenodd"
				clipRule="evenodd"
				d="M5 3H19C20.1 3 21 3.9 21 5V19C21 20.1 20.1 21 19 21H5C3.9 21 3 20.1 3 19V5C3 3.9 3.9 3 5 3ZM6 19H18C18.55 19 19 18.55 19 18V6C19 5.45 18.55 5 18 5H6C5.45 5 5 5.45 5 6V18C5 18.55 5.45 19 6 19Z"
				fill="#454545"
			/>
		</svg>
	);
};

const DefaultIcon = () => {
	return (
		<svg
			xmlns="http://www.w3.org/2000/svg"
			width="24"
			height="24"
			viewBox="0 0 24 24"
			fill="none"
		>
			<path
				fillRule="evenodd"
				clipRule="evenodd"
				d="M5 3H19C20.1 3 21 3.9 21 5V19C21 20.1 20.1 21 19 21H5C3.9 21 3 20.1 3 19V5C3 3.9 3.9 3 5 3ZM6 19H18C18.55 19 19 18.55 19 18V6C19 5.45 18.55 5 18 5H6C5.45 5 5 5.45 5 6V18C5 18.55 5.45 19 6 19Z"
				fill="#BEBEBE"
			/>
		</svg>
	);
};
