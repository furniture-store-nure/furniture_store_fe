export function AuthLayout({ children }: { children: React.ReactNode }) {
	return (
		<div className="relative mt-[50px] flex flex-col items-center">
			<h1 className="font-montserrat text-xl font-bold text-turquoiseDark">
				FURNITURE STORE
			</h1>
			{children}
			<footer className="fixed bottom-0">
				<h2 className="mb-3 text-center text-base tracking-wide text-gray">
					&copy; 2024 Furniture store
				</h2>
			</footer>
		</div>
	);
}
