import { memo, useCallback, useEffect, useMemo } from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import clsx from "classnames";

import { adminRoutes } from "~/config/adminRoutes";
import { userRoutes } from "~/config/userRoutes";
import { useAuth } from "~/hooks";
import { Icon } from "~/components/UI";
import { RoutesConfig } from "~/config/routesConfig";
import { useGetCartsCount } from "~/hooks/carts";
import { useAppDispatch, useAppSelector } from "~/redux/hooks";
import { selectUpdateRequest, setUpdateRequest } from "~/redux/updateRequest";

export const Header = memo(() => {
	const router = useRouter();
	const { isAdmin, session, logout } = useAuth();
	const needsUpdate = useAppSelector(selectUpdateRequest);
	const dispatch = useAppDispatch();
	const { cartsCount, refetch } = useGetCartsCount();

	const isCurrentPath = useCallback(
		(path: string) => router.pathname === path,
		[router.pathname],
	);

	const handleLogOut = useCallback(() => {
		logout();
	}, [logout]);

	const links = useMemo(() => {
		if (!session) {
			return [];
		}

		return (isAdmin ? adminRoutes : userRoutes).filter(
			(link) => link.isShowInHeader,
		);
	}, [isAdmin, session]);

	const handleOpenCart = useCallback(() => {
		router.push(RoutesConfig.Cart);
	}, [router]);

	const handleOpenSettings = useCallback(() => {
		router.push(RoutesConfig.Settings);
	}, [router]);

	const handleOpenComparison = useCallback(() => {
		router.push(RoutesConfig.Comparison);
	}, [router]);

	useEffect(() => {
		if (needsUpdate.includes("countCarts")) {
			refetch();
			dispatch(
				setUpdateRequest({
					needsUpdate: needsUpdate.filter((u) => u !== "countCarts"),
				}),
			);
		}
	}, [dispatch, needsUpdate, refetch]);

	return (
		<header className="fixed top-0 z-30 flex h-[5.625rem] w-full items-center bg-white shadow-card">
			<div className="mx-auto flex w-[80rem] justify-between">
				<div className="flex items-center gap-10">
					<h1 className="cursor-pointer font-montserrat text-xl font-bold text-turquoiseDark">
						<Link href="/">FURNITURE STORE</Link>
					</h1>
					<Icon icon="vertical_divider" />
					<Link
						href={RoutesConfig.Home}
						className={clsx("font-poppins text-base tracking-wide text-gray", {
							"font-semibold": isCurrentPath(RoutesConfig.Home),
						})}
					>
						Головна
					</Link>
					{links.map((link, index) => (
						<Link
							key={`link-${index}`}
							href={link.href}
							className={clsx(
								"font-poppins text-base tracking-wide text-gray",
								{
									"font-semibold": isCurrentPath(link.href),
								},
							)}
						>
							{link.label}
						</Link>
					))}
				</div>
				<div className="flex items-center gap-x-5">
					{!isAdmin && (
						<>
							<span className="flex gap-x-2">
								<Icon icon="mail" />
								<a
									className="text-sm font-semibold tracking-wider text-gray"
									href="mailto:info@furniturestore.com"
								>
									info@furniturestore.com
								</a>
							</span>
							<Icon icon="vertical_divider" />
							<span className="flex items-center gap-x-2">
								<Icon icon="phone" />
								<span className="flex flex-col">
									<a
										className="text-sm font-semibold tracking-wider text-gray"
										href="tel:+380951307716"
									>
										+ 380 95 13 07 716
									</a>
									<span className="text-xs tracking-wider text-grayMedium2">
										Пн-Пят: 10:00-18:00;
									</span>
									<span className="text-xs tracking-wider text-grayMedium2">
										Сб-Нд: 10:00-18:00;
									</span>
								</span>
							</span>
							{session && (
								<>
									<Icon icon="vertical_divider" />
									<div className="relative cursor-pointer" onClick={handleOpenCart}>
										<Icon icon="cart" />
										{cartsCount > 0 && (
											<div className="absolute right-0 top-0 flex size-3 items-center justify-center rounded-full bg-red text-[10px] text-white">
												{cartsCount}
											</div>
										)}
									</div>
									<Icon icon="vertical_divider" />
									<button onClick={handleOpenComparison}>
										<Icon icon="scales" />
									</button>
									<Icon icon="vertical_divider" />
									<Icon
										className="cursor-pointer"
										icon="person"
										onClick={handleOpenSettings}
									/>
									<Icon icon="vertical_divider" />
								</>
							)}
						</>
					)}
					{session ? (
						<button onClick={handleLogOut}>
							<Icon icon="logout" />
						</button>
					) : (
						<Link href={RoutesConfig.Login}>
							<Icon icon="login" />
						</Link>
					)}
				</div>
			</div>
		</header>
	);
});
