import { memo } from "react";
import clsx from "classnames";

import { useAuth } from "~/hooks";
import { Icon } from "~/components/UI";

export const Footer = memo(() => {
	const { isAdmin } = useAuth();

	return (
		<footer className="absolute bottom-0 flex min-h-20 w-full items-center bg-gray">
			<div
				className={clsx("mx-auto flex w-[80rem] flex-col gap-y-5", {
					"mb-7": !isAdmin,
				})}
			>
				{!isAdmin && (
					<>
						<span className="mt-12 flex gap-x-2">
							<Icon icon="place" />{" "}
							<p className="text-base tracking-wider text-grayMedium">
								Харків, проспект Гагаріна, 352
							</p>
						</span>
						<span className="flex gap-x-2">
							<Icon icon="schedule" />{" "}
							<p className="text-base tracking-wider text-grayMedium">
								Пн-Пят 10:00-18:00; Сб 10:00-13:00
							</p>
						</span>
						<span className="flex gap-x-2">
							<Icon icon="phone_gray" />{" "}
							<a
								className="text-base tracking-wider text-grayMedium"
								href="tel:+380951307716"
							>
								Tel.: +380 95 13 07 716
							</a>
						</span>
						<span className="flex gap-x-2">
							<Icon icon="mail_gray" />{" "}
							<a
								className="text-base tracking-wider text-grayMedium"
								href="mailto:info@furniturestore.com"
							>
								Email: info@furniturestore.com
							</a>
						</span>
						<div className="w-full border-t border-white opacity-20"></div>
					</>
				)}
				<h2 className="text-base tracking-wider text-white">
					&copy; 2024 Furniture store
				</h2>
			</div>
		</footer>
	);
});
