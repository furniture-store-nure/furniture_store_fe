import { useCallback, useEffect, useState } from "react";
import clsx from "classnames";

import { Footer, Header } from "./components";
import { useAuth } from "~/hooks";
import { Icon } from "../UI";

export function AppLayout({ children }: { children: React.ReactNode }) {
	const { isAdmin } = useAuth();

	const [isVisible, setIsVisible] = useState<boolean>(false);

	const scrollToTop = useCallback(() => {
		window.scrollTo({ top: 0, behavior: "smooth" });
	}, []);

	const handleScroll = useCallback(() => {
		window.scrollY > 0 ? setIsVisible(true) : setIsVisible(false);
	}, []);

	useEffect(() => {
		window.addEventListener("scroll", handleScroll);
		return () => {
			window.removeEventListener("scroll", handleScroll);
		};
	}, [handleScroll]);

	return (
		<>
			<Header />
			<main>
				<div className="mx-auto mt-[5.625rem] flex w-[80rem] flex-col">
					{children}
				</div>
				<button
					disabled={!isVisible}
					onClick={scrollToTop}
					className={`fixed bottom-32 right-32 size-fit transition-transform ${
						isVisible ? "animate-jump-in" : "opacity-0"
					}`}
				>
					<div className="flex size-14 items-center justify-center rounded-full bg-white shadow-card">
						<Icon icon="up_arrow_green" />
					</div>
				</button>
			</main>
			<div
				className={clsx({
					"pb-[400px]": !isAdmin,
					"pb-[200px]": isAdmin,
				})}
			></div>
			<Footer />
		</>
	);
}
