import { api } from "~/utils/api";
import { useAuth } from "../useAuth.hook";

export const useGetComparisons = () => {
	const { session, isAdmin } = useAuth();
	const { data, isLoading, refetch } = api.comparison.getComparisons.useQuery(
		undefined,
		{
			enabled: !!session && !isAdmin,
		},
	);

  const comparisons = data ?? []

	return { comparisons, isLoading, refetch };
}