import { useCallback } from "react";
import { signOut, useSession } from "next-auth/react";
import { UserRole } from "@prisma/client";

export const useAuth = () => {
	const { status, data: session } = useSession();

	const logout = useCallback(async () => {
		await signOut();
	}, []);

	const userId = session?.user.id ?? "";
	const role = session?.user.role ?? UserRole.USER;
	const isAdmin = role === UserRole.ADMIN;

	return {
		isAdmin,
		userId,
		status,
		role,
		session,
		logout,
	};
};
