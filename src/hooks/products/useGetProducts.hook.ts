import { api } from "~/utils/api";
import type { GetProductsArgs } from "~/server/core/validations";

export const useGetProducts = (args: GetProductsArgs) => {
	const { data, isLoading, refetch } = api.product.getProducts.useQuery(args);

	const count = data?.count ?? 0;

	const products =
		data?.products.map((product) => ({
			...product,
			manufacturerName: product.manufacturer.name,
			editAction: true,
			deleteAction: true,
		})) ?? [];

	return { products, count, isLoading, refetch };
};
