import { api } from "~/utils/api";

export const useGetPopularProducts = () => {
	const { data, isLoading } = api.product.getPopularProducts.useQuery();

	const popularProducts = data ?? [];

	return { popularProducts, isLoading };
};
