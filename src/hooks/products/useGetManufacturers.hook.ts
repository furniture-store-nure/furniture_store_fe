import { useMemo } from "react";

import { api } from "~/utils/api";

export const useGetManufacturers = () => {
	const { data, isLoading } = api.product.getManufacturers.useQuery();

	const manufacturers = useMemo(
		() =>
			data?.map((manufacturer) => ({
				label: manufacturer.name,
				value: manufacturer.id,
			})) || [],
		[data],
	);

	return { manufacturers, isLoading };
};
