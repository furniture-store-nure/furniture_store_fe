import { api } from "~/utils/api";
import type { GetProductByIdArgs } from "~/server/core/validations";

export const useGetProductById = (args: GetProductByIdArgs) => {
	const { data, isLoading, refetch } =
		api.product.getProductById.useQuery(args);

	return { product: data, isLoading, refetch };
};
