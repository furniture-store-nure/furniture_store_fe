import Image from "next/image";
import { type TypeProduct } from "@prisma/client";

import { Icon, type TableColumn } from "~/components/UI";
import { PRODUCT_TYPE_OPTIONS } from "~/utils/constants";

export type ProductTableColumn = {
	id: string;
	logo: string;
	name: string;
	type: TypeProduct;
	manufacturerName: string;
	editAction: boolean;
	deleteAction: boolean;
};

type ProductColumnsProps = {
	handleOpenEditProductModal: (productId: string) => void;
	handleOpenDeleteProductModal: (productId: string) => void;
};

export const useProductColumns = (props: ProductColumnsProps) => {
	const { handleOpenEditProductModal, handleOpenDeleteProductModal } = props;

	const productsColumns: TableColumn<ProductTableColumn>[] = [
		{
			title: "",
			dataIndex: "logo",
			tdClassName: "!h-20 w-[135px] !p-0 !pl-[30px] !pr-[45px]",
			render: (_, record) => (
				<div className="flex h-full flex-col items-center justify-center">
					<Image
						className="size-[60px] shrink-0 rounded-[4px]"
						src={record.logo}
						alt={record.name}
						width={60}
						height={60}
					/>
				</div>
			),
		},
		{
			title: "Назва виробу",
			dataIndex: "name",
			thClassName: "text-left",
			tdClassName: "!h-20 !p-0",
			render: (_, record) => (
				<div className="flex h-full flex-col items-center justify-center">
					<p className="w-full break-words text-xl font-semibold text-gray">
						{record.name}
					</p>
				</div>
			),
		},
		{
			title: "Вид виробу",
			dataIndex: "type",
			thClassName: "text-left",
			tdClassName: "!h-20 !p-0",
			render: (_, record) => (
				<div className="flex h-full flex-col items-center justify-center">
					<p className="w-full break-words text-base tracking-wider text-grayMedium2">
						{
							PRODUCT_TYPE_OPTIONS.find((item) => item.value === record.type)
								?.label
						}
					</p>
				</div>
			),
		},
		{
			title: "Виробник",
			dataIndex: "manufacturerName",
			thClassName: "text-left",
			tdClassName: "!h-20 !p-0",
			render: (_, record) => (
				<div className="flex h-full flex-col items-center justify-center">
					<p className="w-full break-words text-base tracking-wider text-grayMedium2">
						{record.manufacturerName}
					</p>
				</div>
			),
		},
		{
			title: "",
			dataIndex: "editAction",
			tdClassName: "!h-20 w-[84px] !p-0",
			render: (_, record) =>
				record.editAction ? (
					<div className="flex h-full flex-col items-center justify-center">
						<button onClick={() => handleOpenEditProductModal(record.id)}>
							<Icon icon="pen" />
						</button>
					</div>
				) : (
					<></>
				),
		},
		{
			title: "",
			dataIndex: "deleteAction",
			tdClassName: "!h-20 w-[84px] !p-0",
			render: (_, record) =>
				record.deleteAction ? (
					<div className="flex h-full flex-col items-center justify-center">
						<button onClick={() => handleOpenDeleteProductModal(record.id)}>
							<Icon icon="trash" />
						</button>
					</div>
				) : (
					<></>
				),
		},
	];

	return { columns: productsColumns };
};
