import type { Material, Security, TypeProduct } from "@prisma/client";

import { api } from "~/utils/api";

const LIMIT = 4;

type UseGetProductsByScrollProps = {
	manufacturerId?: string;
	type?: TypeProduct;
	material?: Material;
	security?: Security;
	priceFilters?: { priceFrom?: number; priceTo?: number }[];
};

export const useGetProductsByScroll = (props: UseGetProductsByScrollProps) => {
	const { data, fetchNextPage, hasNextPage, isLoading, refetch } =
		api.product.getProductsScroll.useInfiniteQuery(
			{ limit: LIMIT, ...props },
			{
				getNextPageParam: (lastPage) => lastPage.nextCursor,
			},
		);

	const products = data?.pages.flatMap((page) => page.products) ?? [];
	const cursor = data?.pages[data.pages.length - 1]?.nextCursor;
	const count = data?.pages[data.pages.length - 1]?.count ?? 0;

	return {
		products,
		cursor,
		count,
		fetchNextPage,
		hasNextPage,
		isLoading,
		refetch,
	};
};
