import { api } from "~/utils/api";
import { type GetCartArgs } from "~/server/core/validations";

export const useGetCarts = (args: GetCartArgs) => {
	const { data, isLoading, refetch } = api.cart.getCarts.useQuery(args);

	const carts = data ?? [];

	return { carts, isLoading, refetch };
};
