import { api } from "~/utils/api";
import { useAuth } from "../useAuth.hook";

export const useGetCartsCount = () => {
	const { session, isAdmin } = useAuth();
	const { data, isLoading, refetch } = api.cart.getCartsCount.useQuery(
		undefined,
		{
			enabled: !!session && !isAdmin,
		},
	);

	const cartsCount = data ?? 0;

	return { cartsCount, isLoading, refetch };
};
