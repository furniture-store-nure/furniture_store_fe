import { api } from "~/utils/api";
import type { GetOrdersArgs } from "~/server/core/validations";

export const useGetOrders = (args: GetOrdersArgs) => {
	const { data, isLoading, refetch } = api.order.getOrders.useQuery(args);

	const count = data?.count ?? 0;

	const orders =
		data?.orders.map((order) => ({
			...order,
			fullName: `${order.user.lastName} ${order.user.firstName} ${order.user.middleName}`,
			email: order.user.email,
			nameAndCountFurniture: order.carts.map((cart) => ({
				name: cart.product.name,
				quantity: cart.quantity,
			})),
		})) ?? [];

	return { orders, count, isLoading, refetch };
};
