import { OrderStatus, type POSTS } from "@prisma/client";
import clsx from "classnames";

import { ORDER_STATUS_OPTIONS, POST_OPTIONS } from "~/utils/constants";
import { Select, type TableColumn } from "~/components/UI";
import { api } from "~/utils/api";
import { useMessage } from "../useMessage.hook";
import { useAuth } from "../useAuth.hook";

export type OrderTableColumn = {
	id: string;
	shortId: number;
	fullName: string;
	email: string;
	postName: POSTS;
	postOfficeNumber: string | null;
	addressDelivery: string | null;
	nameAndCountFurniture: { name: string; quantity: number }[];
	totalPrice: number;
	status: OrderStatus;
};

type OrderColumnsProps = {
	handleRefetch: () => void;
};

export const useOrderColumns = (props: OrderColumnsProps) => {
	const { handleRefetch } = props;

	const { isAdmin } = useAuth();

	const m = useMessage();

	const { mutate: handleChangeOrderStatus } =
		api.order.changeOrderStatus.useMutation({
			onSuccess: () => {
				m.success("Статус замовлення оновлено");
				handleRefetch();
			},
			onError: (error) => {
				m.error(error.message);
			},
		});

	const ordersColumns: TableColumn<OrderTableColumn>[] = [
		{
			title: "Номер",
			dataIndex: "shortId",
			tdClassName: "w-[6.25rem]",
			render: (_, record) => (
				<div className="flex items-center justify-center">
					<p className="truncate text-sm font-semibold tracking-wider">
						{record.shortId}
					</p>
				</div>
			),
		},
		{
			title: "ФIО клiента",
			dataIndex: "fullName",
			tdClassName: "w-[13.75rem]",
			render: (_, record) => (
				<p className="w-full break-words">{record.fullName}</p>
			),
		},
		{
			title: "Ел. пошта",
			dataIndex: "email",
			render: (_, record) => <p className="truncate">{record.email}</p>,
		},
		{
			title: "Назва пошти",
			dataIndex: "postName",
			tdClassName: "w-[8.125rem]",
			render: (_, record) => (
				<p className="break-all text-center">
					{POST_OPTIONS.find((item) => item.value === record.postName)?.label}
				</p>
			),
		},
		{
			title: "Номер відділення",
			dataIndex: "postOfficeNumber",
			tdClassName: "w-[7.5rem]",
			render: (_, record) => (
				<p className="truncate text-center">{record.postOfficeNumber}</p>
			),
		},
		{
			title: "Адреса доставки",
			dataIndex: "addressDelivery",
			tdClassName: "w-[7.5rem]",
			render: (_, record) => (
				<p className="break-all">{record.addressDelivery}</p>
			),
		},
		{
			title: "Назви меблiв та кiлькiсть",
			dataIndex: "nameAndCountFurniture",
			tdClassName: "w-[7.5rem]",
			render: (_, record) => (
				<>
					<div className="flex flex-col gap-y-2">
						{record.nameAndCountFurniture.map((item, index) => (
							<p
								className="break-words text-base font-semibold text-gray"
								key={`furniture_${index}`}
							>
								{item.name} x {item.quantity}
							</p>
						))}
					</div>
				</>
			),
		},
		{
			title: "Загальна вартiсть",
			dataIndex: "totalPrice",
			tdClassName: "w-[7.5rem]",
			render: (_, record) => (
				<p className="break-all text-center text-xl font-semibold text-red">
					{record.totalPrice.toLocaleString("ru-RU")} &#8372;
				</p>
			),
		},
		{
			title: "Статус",
			dataIndex: "status",
			tdClassName: "w-[8.125rem]",
			render: (_, record) =>
				record.status === OrderStatus.NEW && isAdmin ? (
					<Select
						className="w-[120px] !p-3"
						options={ORDER_STATUS_OPTIONS.filter(
							(option) => option.value !== OrderStatus.NEW,
						)}
						value={
							ORDER_STATUS_OPTIONS.find((item) => item.value === record.status)!
						}
						onChange={(value) =>
							handleChangeOrderStatus({
								orderId: record.id,
								status: value.value,
							})
						}
					/>
				) : (
					<p
						className={clsx(
							"text-center text-sm font-semibold tracking-wider",
							{
								"text-red": record.status === OrderStatus.REJECTED,
								"text-green": record.status === OrderStatus.FINISHED,
							},
						)}
					>
						{
							ORDER_STATUS_OPTIONS.find((item) => item.value === record.status)
								?.label
						}
					</p>
				),
		},
	];

	return { ordersColumns };
};
