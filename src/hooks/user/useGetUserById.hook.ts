import { api } from "~/utils/api";

export const useGetUserById = () => {
	const { data, refetch } = api.auth.getUserById.useQuery();

	return { user: data, refetch };
};
