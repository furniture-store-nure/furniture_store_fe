import {
	Material,
	OrderStatus,
	POSTS,
	Security,
	TypeProduct,
} from "@prisma/client";

export const ORDER_STATUS_OPTIONS = [
	{ label: "Нове", value: OrderStatus.NEW },
	{ label: "Скасовано", value: OrderStatus.REJECTED },
	{ label: "Завершено", value: OrderStatus.FINISHED },
];

export const POST_OPTIONS = [
	{ label: "Нова пошта", value: POSTS.NovaPoshta },
	{ label: "Укр. пошта", value: POSTS.UkrPoshta },
];

export const PRODUCT_TYPE_OPTIONS = [
	{ label: "Ліжко", value: TypeProduct.BED },
	{ label: "Стілець", value: TypeProduct.CHAIR },
	{ label: "Диван", value: TypeProduct.SOFA },
];

export const MATERIAL_OPTIONS = [
	{ label: "Шкіра", value: Material.LEATHER },
	{ label: "Тканина", value: Material.TEXTILE },
];

export const SECURITY_OPTIONS = [
	{ label: "Безпечно для дітей", value: Security.SAFE_FOR_CHILDREN },
	{ label: "Містить дрібні деталі", value: Security.CONTAINS_SMALL_DETAILS },
];

export enum SettingsTab {
	Info = "info",
	Password = "password",
	Email = "email",
}

export enum DeliveryType {
	PostOfficeNumberCheck = "postOfficeNumberCheck",
	AddressDeliveryCheck = "addressDeliveryCheck",
}
