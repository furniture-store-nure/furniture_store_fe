export const FileToBase64 = (file: Blob) =>
	new Promise((resolve, reject) => {
		const reader = new FileReader();
		reader.readAsDataURL(file);
		reader.onload = () => resolve(reader.result);
		reader.onerror = reject;
	});

export const Base64ToFile = (base64: string, filename: string) => {
	let arr = base64.split(","),
		mime = arr[0]?.match(/:(.*?);/)?.[1],
		bstr = atob(arr[arr.length - 1]!),
		n = bstr.length,
		u8arr = new Uint8Array(n);
	while (n--) {
		u8arr[n] = bstr.charCodeAt(n);
	}
	return new File([u8arr], filename, { type: mime });
};
