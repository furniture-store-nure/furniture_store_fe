import type { GetServerSidePropsContext } from "next";
import type { Session } from "next-auth";
import { createServerSideHelpers } from "@trpc/react-query/server";
import { UserRole } from "@prisma/client";
import superjson from "superjson";

import { RoutesConfig } from "../config/routesConfig";
import { appRouter } from "~/server/api/root";
import { userRoutes } from "~/config/userRoutes";
import { adminRoutes } from "~/config/adminRoutes";
import { publicRoutes } from "~/config/publicRoutes";

export function checkAccess(
	session: Session | null,
	context: GetServerSidePropsContext,
) {
	const path = context.resolvedUrl.split("?")[0] ?? "";

	if (publicRoutes.includes(path)) {
		return null;
	}

	if (session?.user) {
		const role = session?.user.role;

		const isAdmin = role === UserRole.ADMIN;

		const userRouteValues = userRoutes.map((route) => route.href);
		const adminRouteValues = adminRoutes.map((route) => route.href);

		// If the user is already logged in, do not allow them to go to login or registration pages.
		if ([RoutesConfig.Login, RoutesConfig.Registration].includes(path)) {
			if (isAdmin) {
				return redirectTo(RoutesConfig.Orders);
			}

			return redirectTo(RoutesConfig.Home);
		}

		// Verify access to static routers.
		if (!userRouteValues.includes(path) && !isAdmin) {
			return redirectTo(RoutesConfig.Home);
		}

		if (isAdmin && !adminRouteValues.includes(path)) {
			return redirectTo(RoutesConfig.Orders);
		}
	} else {
		// For unauthorized users, restrict access to pages other than those specified.
		if (![RoutesConfig.Login, RoutesConfig.Registration].includes(path)) {
			return redirectTo(RoutesConfig.Login);
		}
	}

	return null;
}

function redirectTo(destination: string) {
	return {
		redirect: {
			destination,
			permanent: false,
		},
	};
}

export const getServerProxySSGHelpers = (session: Session | null) => {
	return createServerSideHelpers({
		router: appRouter,
		ctx: {
			user: session?.user,
			session,
			ip: null as any,
			res: null as any,
			cache: null as any,
			req: null as any,
		} as any,
		transformer: superjson,
	});
};
