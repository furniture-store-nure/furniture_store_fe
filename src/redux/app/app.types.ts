export type ModalType =
	| "create_product"
	| "edit_product"
	| "delete_product"
	| null;

export interface AppState {
	showModal?: ModalType;
}
