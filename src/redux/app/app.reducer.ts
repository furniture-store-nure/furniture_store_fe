import type { PayloadAction } from "@reduxjs/toolkit";
import { createSlice } from "@reduxjs/toolkit";
import type { AppState, ModalType } from "./app.types";

const initialState: AppState = {
	showModal: null,
};

export const appSlice = createSlice({
	name: "app",
	initialState,
	reducers: {
		setOpenModal: (state, action: PayloadAction<ModalType>) => {
			state.showModal = action.payload;
		},
	},
});

export const { setOpenModal } = appSlice.actions;

export default appSlice.reducer;
