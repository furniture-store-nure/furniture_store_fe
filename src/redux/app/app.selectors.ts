import type { RootState } from "../store";

export const selectShowModal = (state: RootState) => state.app.showModal;
