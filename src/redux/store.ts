import { combineReducers, configureStore } from "@reduxjs/toolkit";

import appReducer from "./app/app.reducer";
import updateRequestReducer from "./updateRequest/updateRequest.reducer";

const rootReducer = combineReducers({
	app: appReducer,
	updateRequest: updateRequestReducer,
});

const devMode = process.env.NODE_ENV === "development";

export const store = configureStore({
	devTools: devMode,
	reducer: rootReducer,
});

export type RootState = ReturnType<typeof rootReducer>;
export type AppDispatch = typeof store.dispatch;
