type RequestType = "cart" | "orders" | "countCarts" | "comparisons" | [];

export type UpdateRequestState = {
	needsUpdate: RequestType[];
};
