import type { RootState } from "../store";

export const selectUpdateRequest = (state: RootState) =>
	state.updateRequest.needsUpdate;
