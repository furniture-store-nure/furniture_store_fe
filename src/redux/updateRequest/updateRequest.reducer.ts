import type { PayloadAction } from "@reduxjs/toolkit";
import { createSlice } from "@reduxjs/toolkit";

import type { UpdateRequestState } from "./updateRequest.types";

const initialState: UpdateRequestState = {
	needsUpdate: [],
};

export const updateRequestSlice = createSlice({
	name: "updateRequest",
	initialState,
	reducers: {
		setUpdateRequest: (state, action: PayloadAction<UpdateRequestState>) => {
			state.needsUpdate = action.payload.needsUpdate;
		},
	},
});

export const { setUpdateRequest } = updateRequestSlice.actions;

export default updateRequestSlice.reducer;
