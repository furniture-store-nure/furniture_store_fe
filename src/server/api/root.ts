import { createTRPCRouter } from "~/server/api/trpc";
import {
	authRouter,
	cartRouter,
	comparisonRouter,
	orderRouter,
	productRouter,
} from "./routers";

/**
 * This is the primary router for your server.
 *
 * All routers added in /api/routers should be manually added here.
 */
export const appRouter = createTRPCRouter({
	auth: authRouter,
	order: orderRouter,
	product: productRouter,
	cart: cartRouter,
	comparison: comparisonRouter,
});

// export type definition of API
export type AppRouter = typeof appRouter;
