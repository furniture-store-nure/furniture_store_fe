import { z } from "zod";

import { container } from "~/server/core/inversify.config";
import { userProcedure, createTRPCRouter } from "../trpc";
import { SYMBOLS } from "~/server/constants/symbols";
import { type ICartService } from "~/server/core/interfaces/ICartService";
import {
	createCartSchema,
	getCartSchema,
	updateCartSchema,
} from "~/server/core/validations";

const CartService = container.get<ICartService>(SYMBOLS.ICartService);

export const cartRouter = createTRPCRouter({
	getCartsCount: userProcedure.query(async ({ ctx }) => {
		return CartService.getCartsCount({ userId: ctx.session.user.id });
	}),

	getCarts: userProcedure.input(getCartSchema).query(async ({ input }) => {
		return CartService.getCarts(input);
	}),

	createCart: userProcedure
		.input(createCartSchema)
		.mutation(async ({ input }) => {
			return CartService.createCart(input);
		}),

	updateCart: userProcedure
		.input(updateCartSchema)
		.mutation(async ({ input }) => {
			return CartService.updateCart(input);
		}),

	deleteCart: userProcedure.input(z.string()).mutation(async ({ input }) => {
		return CartService.deleteCart(input);
	}),
});
