import { container } from "~/server/core/inversify.config";
import { adminProcedure, createTRPCRouter, publicProcedure } from "../trpc";
import { SYMBOLS } from "~/server/constants/symbols";
import {
	productSchema,
	getProductByIdSchema,
	getProductsSchema,
	editProductSchema,
	deleteProductSchema,
	getProductsScrollSchema,
} from "~/server/core/validations";
import type { IProductService } from "~/server/core/interfaces/IProductService";

const ProductService = container.get<IProductService>(SYMBOLS.IProductService);

export const productRouter = createTRPCRouter({
	getProducts: adminProcedure
		.input(getProductsSchema)
		.query(async ({ input }) => {
			return ProductService.getProducts(input);
		}),

	getProductsScroll: publicProcedure
		.input(getProductsScrollSchema)
		.query(async ({ input }) => {
			return ProductService.getProductsScroll(input);
		}),

	getPopularProducts: publicProcedure.query(async () => {
		return ProductService.getPopularProducts();
	}),

	getProductById: adminProcedure
		.input(getProductByIdSchema)
		.query(async ({ input }) => {
			return ProductService.getProductById(input);
		}),

	getManufacturers: publicProcedure.query(async () => {
		return ProductService.getManufacturers();
	}),

	createProduct: adminProcedure
		.input(productSchema)
		.mutation(async ({ input }) => {
			return ProductService.createProduct(input);
		}),

	deleteProduct: adminProcedure
		.input(deleteProductSchema)
		.mutation(async ({ input }) => {
			return ProductService.deleteProduct(input);
		}),

	editProduct: adminProcedure
		.input(editProductSchema)
		.mutation(async ({ input }) => {
			return ProductService.editProduct(input);
		}),
});
