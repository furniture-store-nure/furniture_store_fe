import { container } from "~/server/core/inversify.config";
import { createTRPCRouter, publicProcedure, userProcedure } from "../trpc";
import { SYMBOLS } from "~/server/constants/symbols";
import {
	signInSchema,
	signUpSchema,
	updateUserEmailSchema,
	updateUserPasswordSchema,
	updateUserSchema,
} from "~/server/core/validations";
import type { IUserService } from "~/server/core/interfaces/IUserService";
import type { IAuthService } from "~/server/core/interfaces/IAuthService";

const UserService = container.get<IUserService>(SYMBOLS.IUserService);
const AuthService = container.get<IAuthService>(SYMBOLS.IAuthService);

export const authRouter = createTRPCRouter({
	signUp: publicProcedure.input(signUpSchema).mutation(async ({ input }) => {
		return UserService.createUser(input);
	}),

	signIn: publicProcedure.input(signInSchema).mutation(async ({ input }) => {
		return AuthService.signIn(input);
	}),

	updateUser: userProcedure
		.input(updateUserSchema)
		.mutation(async ({ ctx, input }) => {
			return UserService.updateUser({
				userId: ctx.session.user.id,
				...input,
			});
		}),

	getUserById: userProcedure.query(async ({ ctx }) => {
		return UserService.getUserById(ctx.session.user.id);
	}),

	changePassword: userProcedure
		.input(updateUserPasswordSchema)
		.mutation(async ({ ctx, input }) => {
			return UserService.updateUserPassword({
				userId: ctx.session.user.id,
				...input,
			});
		}),

	changeEmail: userProcedure
		.input(updateUserEmailSchema)
		.mutation(async ({ ctx, input }) => {
			return UserService.updateUserEmail({
				userId: ctx.session.user.id,
				...input,
			});
		}),
});
