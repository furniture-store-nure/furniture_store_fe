import { z } from "zod";

import { container } from "~/server/core/inversify.config";
import { userProcedure, createTRPCRouter } from "../trpc";
import { SYMBOLS } from "~/server/constants/symbols";
import { type IComparisonService } from "~/server/core/interfaces/IComparisonService";

const ComparisonService = container.get<IComparisonService>(
	SYMBOLS.IComparisonService,
);

export const comparisonRouter = createTRPCRouter({
	createComparison: userProcedure
		.input(z.string())
		.mutation(async ({ input, ctx }) => {
			return ComparisonService.createComparison({
				productId: input,
				userId: ctx.session.user.id,
			});
		}),

	getComparisons: userProcedure.query(async ({ ctx }) => {
		return ComparisonService.getComparisons({ userId: ctx.session.user.id });
	}),

	deleteComparisons: userProcedure.mutation(async ({ ctx }) => {
		return ComparisonService.deleteComparisons({
			userId: ctx.session.user.id,
		});
	}),
});
