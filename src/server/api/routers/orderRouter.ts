import { container } from "~/server/core/inversify.config";
import { adminProcedure, createTRPCRouter, protectedProcedure, userProcedure } from "../trpc";
import { SYMBOLS } from "~/server/constants/symbols";
import type { IOrderService } from "~/server/core/interfaces/IOrderService";
import {
	changeOrderStatusSchema,
	createOrderSchema,
	getOrdersSchema,
} from "~/server/core/validations";

const OrderService = container.get<IOrderService>(SYMBOLS.IOrderService);

export const orderRouter = createTRPCRouter({
	getOrders: protectedProcedure.input(getOrdersSchema).query(async ({ input, ctx }) => {
		return OrderService.getOrders({ ...input, userId: ctx.session.user.id });
	}),

	createOrder: userProcedure
		.input(createOrderSchema)
		.mutation(async ({ input }) => {
			return OrderService.createOrder(input);
		}),

	changeOrderStatus: adminProcedure
		.input(changeOrderStatusSchema)
		.mutation(async ({ input }) => {
			return OrderService.changeOrderStatus(input);
		}),
});
