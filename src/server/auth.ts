import { type GetServerSidePropsContext } from "next";
import {
	getServerSession,
	type NextAuthOptions,
	type DefaultSession,
} from "next-auth";
import CredentialsProvider from "next-auth/providers/credentials";

import { env } from "~/env.mjs";
import { type SessionUser } from "./core/interfaces/SessionUser";
import { signInSchema } from "./core/validations";
import { authorizeUser } from "./core/useCases/auth";

/**
 * Module augmentation for `next-auth` types. Allows us to add custom properties to the `session`
 * object and keep type safety.
 *
 * @see https://next-auth.js.org/getting-started/typescript#module-augmentation
 */

declare module "next-auth" {
	interface Session extends DefaultSession {
		user: SessionUser;
	}
}

/**
 * Options for NextAuth.js used to configure adapters, providers, callbacks, etc.
 *
 * @see https://next-auth.js.org/configuration/options
 */
export const authOptions: NextAuthOptions = {
	callbacks: {
		session: ({ session, token }) => {
			if (token?.user) {
				return {
					...session,
					user: {
						...session?.user,
						...token.user,
					},
				};
			}

			return { ...session };
		},
		jwt({ token, user }) {
			if (user) return { ...token, user };
			return token;
		},
	},
	providers: [
		CredentialsProvider({
			id: "credentials",
			name: "Credentials",
			credentials: {
				email: {
					label: "Email",
					type: "text",
					placeholder: "email@example.com",
				},
				password: { label: "Password", type: "password" },
			},
			async authorize(credentials) {
				const creds = await signInSchema.parseAsync(credentials);
				return authorizeUser(creds);
			},
		}),
	],
	secret: env.NEXTAUTH_SECRET,
	session: {
		strategy: "jwt",
	},
};

/**
 * Wrapper for `getServerSession` so that you don't need to import the `authOptions` in every file.
 *
 * @see https://next-auth.js.org/configuration/nextjs
 */
export const getServerAuthSession = (ctx: {
	req: GetServerSidePropsContext["req"];
	res: GetServerSidePropsContext["res"];
}) => {
	return getServerSession(ctx.req, ctx.res, authOptions);
};
