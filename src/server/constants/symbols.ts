export const SYMBOLS = {
	IAuthService: Symbol.for("IAuthService"),
	IUserService: Symbol.for("IUserService"),
	IOrderService: Symbol.for("IOrderService"),
	IProductService: Symbol.for("IProductService"),
	ICartService: Symbol.for("ICartService"),
	IComparisonService: Symbol.for("IComparisonService"),
};
