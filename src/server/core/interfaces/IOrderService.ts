import type { Cart, Order, Product, User } from "@prisma/client";

import type {
	GetOrdersArgs,
	ChangeOrderStatusArgs,
	CreateOrderArgs,
} from "../validations";

export interface IOrderService {
	getOrders: (args: GetOrdersArgs & { userId: string }) => Promise<{
		orders: (Order & { user: User; carts: (Cart & { product: Product })[] })[];
		count: number;
	}>;
	createOrder(args: CreateOrderArgs): Promise<Order>;
	changeOrderStatus: (args: ChangeOrderStatusArgs) => Promise<Order>;
}
