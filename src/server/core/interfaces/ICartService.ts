import type { Cart, Product } from "@prisma/client";

import type {
	CreateCartArgs,
	GetCartArgs,
	UpdateCartArgs,
} from "../validations";

export interface ICartService {
	getCartsCount(args: GetCartArgs): Promise<number>;
	getCarts(args: GetCartArgs): Promise<(Cart & { product: Product })[]>;
	createCart(args: CreateCartArgs): Promise<Cart>;
	updateCart(args: UpdateCartArgs): Promise<Cart>;
	deleteCart(cartId: string): Promise<void>;
}
