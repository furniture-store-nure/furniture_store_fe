import type { Manufacturer, Product } from "@prisma/client";

import type {
	ProductArgs,
	GetProductByIdArgs,
	GetProductsArgs,
	EditProductArgs,
	DeleteProductArgs,
	GetProductsScrollArgs,
} from "../validations";

export interface IProductService {
	getProducts(args: GetProductsArgs): Promise<{
		products: (Product & { manufacturer: Manufacturer })[];
		count: number;
	}>;
	getProductsScroll(args: GetProductsScrollArgs): Promise<{
		products: (Product & { manufacturer: Manufacturer })[];
		nextCursor: string | null;
		count: number;
	}>;
	getPopularProducts(): Promise<Product[]>;
	getProductById(args: GetProductByIdArgs): Promise<Product>;
	getManufacturers(): Promise<Manufacturer[]>;
	createProduct(args: ProductArgs): Promise<Product>;
	deleteProduct(args: DeleteProductArgs): Promise<void>;
	editProduct(args: EditProductArgs): Promise<Product>;
}
