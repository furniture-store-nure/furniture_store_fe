import type { SignInArgs } from "../validations";
import type { SessionUser } from "./SessionUser";

export interface IAuthService {
	signIn({ email, password }: SignInArgs): Promise<SessionUser>;
}
