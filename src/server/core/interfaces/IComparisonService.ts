import type { Product, Comparison, Manufacturer } from "@prisma/client";

import type {
	CreateComparisonArgs,
	GetComparisonsArgs,
	DeleteComparisonArgs,
} from "~/server/core/validations";

type ProductWithManufacturer = Product & {
	manufacturer: Manufacturer;
};
type ComparisonWithProduct = Comparison & {
	product: ProductWithManufacturer;
};

export interface IComparisonService {
	createComparison: (args: CreateComparisonArgs) => Promise<Comparison>;
	getComparisons: (
		args: GetComparisonsArgs,
	) => Promise<ComparisonWithProduct[]>;
	deleteComparisons: (args: DeleteComparisonArgs) => Promise<void>;
}
