import type { User } from "@prisma/client";

import type { SessionUser } from "./SessionUser";
import type {
	SignUpArgs,
	UpdateUserArgs,
	UpdateUserEmailArgs,
	UpdateUserPasswordArgs,
} from "../validations";

export interface IUserService {
	createUser(args: SignUpArgs): Promise<User | null>;
	getUserById(userId: string): Promise<User | null>;
	updateUser(args: UpdateUserArgs & { userId: string }): Promise<User | null>;
	updateUserPassword(
		args: UpdateUserPasswordArgs & { userId: string },
	): Promise<User>;
	updateUserEmail(
		args: UpdateUserEmailArgs & { userId: string },
	): Promise<User>;
	getUserByEmail(email: string): Promise<User | null>;
	hashPassword(password: string): Promise<string>;
	verifyPassword(user: User, password: string): Promise<boolean>;
	normalizeUserForSession(user: User): SessionUser;
}
