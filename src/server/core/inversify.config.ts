import "reflect-metadata";
import { Container } from "inversify";

import type { IUserService } from "./interfaces/IUserService";
import { SYMBOLS } from "../constants/symbols";
import {
	OrderService,
	AuthService,
	UserService,
	ProductService,
	CartService,
	ComparisonService,
} from "./services";
import type { IOrderService } from "./interfaces/IOrderService";
import type { IAuthService } from "./interfaces/IAuthService";
import type { IProductService } from "./interfaces/IProductService";
import type { ICartService } from "./interfaces/ICartService";
import type { IComparisonService } from "./interfaces/IComparisonService";

const container = new Container();

container.bind<IUserService>(SYMBOLS.IUserService).to(UserService);
container.bind<IAuthService>(SYMBOLS.IAuthService).to(AuthService);
container.bind<IOrderService>(SYMBOLS.IOrderService).to(OrderService);
container.bind<IProductService>(SYMBOLS.IProductService).to(ProductService);
container.bind<ICartService>(SYMBOLS.ICartService).to(CartService);
container
	.bind<IComparisonService>(SYMBOLS.IComparisonService)
	.to(ComparisonService);

export { container };
