import { Cart, Product, type PrismaClient } from "@prisma/client";
import { TRPCError } from "@trpc/server";
import { injectable } from "inversify";

import { ICartService } from "../interfaces/ICartService";
import { prisma } from "~/server/db";
import { CreateCartArgs, GetCartArgs, UpdateCartArgs } from "../validations";

@injectable()
export class CartService implements ICartService {
	protected prismaTx: PrismaClient;

	constructor() {
		this.prismaTx = prisma;
	}

	async getCartsCount(args: GetCartArgs): Promise<number> {
		return this.prismaTx.cart.count({
			where: { ...args, orderId: null },
		});
	}

	async getCarts(args: GetCartArgs): Promise<(Cart & { product: Product })[]> {
		return this.prismaTx.cart.findMany({
			include: { product: true },
			where: { ...args, orderId: null },
			orderBy: { createdAt: "desc" },
		});
	}

	async createCart(args: CreateCartArgs): Promise<Cart> {
		const { productId, userId } = args;

		const product = await this.prismaTx.product.findUnique({
			where: { id: productId },
		});

		if (!product) {
			throw new TRPCError({ code: "NOT_FOUND", message: "Товар не знайдено" });
		}

		const user = await this.prismaTx.user.findUnique({
			include: { carts: true },
			where: { id: userId },
		});

		if (!user) {
			throw new TRPCError({
				code: "NOT_FOUND",
				message: "Користувач не знайдено",
			});
		}

		if (
			user.carts.some((c) => c.productId === productId && c.orderId === null)
		) {
			throw new TRPCError({
				code: "BAD_REQUEST",
				message: "Товар вже додано в кошик",
			});
		}

		return this.prismaTx.cart.create({
			data: {
				productId,
				userId,
				quantity: 1,
			},
		});
	}

	async updateCart(args: UpdateCartArgs): Promise<Cart> {
		const { cartId, quantity, orderId } = args;

		const cart = await this.prismaTx.cart.findUnique({
			where: { id: cartId },
		});

		if (!cart) {
			throw new TRPCError({ code: "NOT_FOUND", message: "Кошик не знайдено" });
		}

		return this.prismaTx.cart.update({
			where: { id: cartId },
			data: { quantity, orderId },
		});
	}

	async deleteCart(cartId: string): Promise<void> {
		const cart = await this.prismaTx.cart.findUnique({
			where: { id: cartId },
		});

		if (!cart) {
			throw new TRPCError({ code: "NOT_FOUND", message: "Кошик не знайдено" });
		}

		await this.prismaTx.cart.delete({
			where: { id: cartId },
		});
	}
}
