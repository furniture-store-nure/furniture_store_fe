import {
	type PrismaClient,
	Product,
	Manufacturer,
	Prisma,
	TypeProduct,
} from "@prisma/client";
import { injectable } from "inversify";
import { TRPCError } from "@trpc/server";

import { prisma } from "~/server/db";
import { IProductService } from "../interfaces/IProductService";
import {
	ProductArgs,
	GetProductByIdArgs,
	GetProductsArgs,
	EditProductArgs,
	DeleteProductArgs,
	GetProductsScrollArgs,
} from "../validations";

@injectable()
export class ProductService implements IProductService {
	protected prismaTx: PrismaClient;

	constructor() {
		this.prismaTx = prisma;
	}

	async getProducts(args: GetProductsArgs): Promise<{
		products: (Product & { manufacturer: Manufacturer })[];
		count: number;
	}> {
		const { page, limit } = args;

		return this.prismaTx.$transaction(async (tx) => {
			const products = await tx.product.findMany({
				include: {
					manufacturer: true,
				},
				take: limit,
				skip: (page - 1) * limit,
				orderBy: {
					createdAt: "desc",
				},
			});

			const count = await tx.product.count();

			return { products, count };
		});
	}

	async getProductsScroll(args: GetProductsScrollArgs): Promise<{
		products: (Product & { manufacturer: Manufacturer })[];
		nextCursor: string | null;
		count: number;
	}> {
		const {
			limit,
			cursor,
			manufacturerId,
			type,
			material,
			security,
			priceFilters,
		} = args;

		const where: Prisma.ProductWhereInput = {};

		if (manufacturerId) {
			const manufacturer = await this.prismaTx.manufacturer.findUnique({
				where: { id: manufacturerId },
			});

			if (!manufacturer) {
				throw new TRPCError({
					code: "NOT_FOUND",
					message: "Виробник не знайдений",
				});
			}

			where.manufacturerId = manufacturerId;
		}

		if (type) {
			where.type = type;
		}

		if (material) {
			where.material = material;
		}

		if (security) {
			where.security = security;
		}

		if (priceFilters && priceFilters.length > 0) {
			where.OR = priceFilters.map((priceFilter) => {
				const filter: any = {};

				const priceFrom = priceFilter.priceFrom;
				const priceTo = priceFilter.priceTo;

				if (priceFrom === undefined && priceTo === undefined) {
					throw new TRPCError({
						code: "BAD_REQUEST",
						message: "Ціна не вказана",
					});
				}

				if (priceFrom !== undefined) {
					filter.gte = priceFrom;
				}

				if (priceTo !== undefined) {
					filter.lte = priceTo;
				}
				return { price: filter };
			});
		}

		return this.prismaTx.$transaction(async (tx) => {
			const products = await tx.product.findMany({
				include: {
					manufacturer: true,
				},
				take: limit + 1,
				skip: cursor ? 1 : 0,
				cursor: cursor ? { id: cursor } : undefined,
				orderBy: {
					createdAt: "desc",
				},
				where,
			});

			let nextCursor: string | null = null;

			if (products.length > limit) {
				const lastProduct = products.pop();
				nextCursor = lastProduct?.id || null;
			}

			const count = await tx.product.count({
				where,
			});

			return { products, nextCursor, count };
		});
	}

	async getPopularProducts(): Promise<Product[]> {
		let getPopularProducts = [];

		const popularChair = await this.prismaTx.product.findFirst({
			where: {
				type: TypeProduct.CHAIR,
				carts: {
					some: {
						orderId: {
							not: null,
						},
					},
				},
			},
			orderBy: {
				carts: {
					_count: "desc",
				},
			},
		});

		const popularSofa = await this.prismaTx.product.findFirst({
			where: {
				type: TypeProduct.SOFA,
				carts: {
					some: {
						orderId: {
							not: null,
						},
					},
				},
			},
			orderBy: {
				carts: {
					_count: "desc",
				},
			},
		});

		const popularBed = await this.prismaTx.product.findFirst({
			where: {
				type: TypeProduct.BED,
				carts: {
					some: {
						orderId: {
							not: null,
						},
					},
				},
			},
			orderBy: {
				carts: {
					_count: "desc",
				},
			},
		});

		if (popularChair) {
			getPopularProducts.push(popularChair);
		}

		if (popularSofa) {
			getPopularProducts.push(popularSofa);
		}

		if (popularBed) {
			getPopularProducts.push(popularBed);
		}

		return getPopularProducts;
	}

	async getProductById(args: GetProductByIdArgs): Promise<Product> {
		const { productId } = args;

		const product = await this.prismaTx.product.findUnique({
			where: {
				id: productId,
			},
		});

		if (!product) {
			throw new TRPCError({
				code: "NOT_FOUND",
				message: "Товар не знайдено",
			});
		}

		return product;
	}

	async getManufacturers(): Promise<Manufacturer[]> {
		return this.prismaTx.manufacturer.findMany({
			orderBy: {
				name: "desc",
			},
		});
	}

	async createProduct(args: ProductArgs): Promise<Product> {
		return this.prismaTx.product.create({
			data: {
				...args,
			},
		});
	}

	async deleteProduct(args: DeleteProductArgs): Promise<void> {
		const { productId } = args;

		const product = await this.prismaTx.product.findUnique({
			select: {
				carts: true,
			},
			where: {
				id: productId,
			},
		});

		if (!product) {
			throw new TRPCError({
				code: "NOT_FOUND",
				message: "Товар не знайдено",
			});
		}

		if (product.carts.find((cart) => cart.orderId !== null)) {
			throw new TRPCError({
				code: "FORBIDDEN",
				message: "Товар вже замовлено",
			});
		}

		await this.prismaTx.product.delete({
			where: {
				id: productId,
			},
		});
	}

	async editProduct(args: EditProductArgs): Promise<Product> {
		const product = await this.prismaTx.product.findUnique({
			where: {
				id: args.id,
			},
		});

		if (!product) {
			throw new TRPCError({
				code: "NOT_FOUND",
				message: "Товар не знайдено",
			});
		}

		return this.prismaTx.product.update({
			where: {
				id: args.id,
			},
			data: {
				...args,
			},
		});
	}
}
