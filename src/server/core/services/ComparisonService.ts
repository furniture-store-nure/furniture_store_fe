import { type PrismaClient } from "@prisma/client";
import { injectable } from "inversify";
import { TRPCError } from "@trpc/server";

import { IComparisonService } from "../interfaces/IComparisonService";
import { prisma } from "~/server/db";
import {
	CreateComparisonArgs,
	DeleteComparisonArgs,
	GetComparisonsArgs,
} from "../validations";

@injectable()
export class ComparisonService implements IComparisonService {
	protected prismaTx: PrismaClient;

	constructor() {
		this.prismaTx = prisma;
	}

	async createComparison(args: CreateComparisonArgs) {
		const { productId, userId } = args;

		const product = await this.prismaTx.product.findUnique({
			where: {
				id: productId,
			},
		});

		if (!product) {
			throw new TRPCError({
				code: "NOT_FOUND",
				message: "Товар не знайдено",
			});
		}

		const user = await this.prismaTx.user.findUnique({
			include: {
				comparisons: true,
			},
			where: {
				id: userId,
			},
		});

		if (!user) {
			throw new TRPCError({
				code: "NOT_FOUND",
				message: "Користувача не знайдено",
			});
		}

		if (user.comparisons.length === 2) {
			throw new TRPCError({
				code: "BAD_REQUEST",
				message: "Користувач має вже 2 порівняння",
			});
		}

		if (user.comparisons.some((comparison) => comparison.productId === productId)) {
			throw new TRPCError({
				code: "BAD_REQUEST",
				message: "Товар вже доданий до порівняння",
			});
		}

		return this.prismaTx.comparison.create({
			data: args,
		});
	}

	async getComparisons(args: GetComparisonsArgs) {
		const { userId } = args;

		const user = await this.prismaTx.user.findUnique({
			include: {
				comparisons: true,
			},
			where: {
				id: userId,
			},
		});

		if (!user) {
			throw new TRPCError({
				code: "NOT_FOUND",
				message: "Користувача не знайдено",
			});
		}

		return this.prismaTx.comparison.findMany({
			include: {
				product: {
					include: {
						manufacturer: true,
					},
				},
			},
			where: {
				userId: userId,
			},
		});
	}

	async deleteComparisons(args: DeleteComparisonArgs) {
		const { userId } = args;

		const user = await this.prismaTx.user.findUnique({
			include: {
				comparisons: true,
			},
			where: {
				id: userId,
			},
		});

		if (!user) {
			throw new TRPCError({
				code: "NOT_FOUND",
				message: "Користувача не знайдено",
			});
		}

		if (user.comparisons.length === 0) {
			throw new TRPCError({
				code: "NOT_FOUND",
				message: "Порівняння не знайдено",
			});
		}

		await this.prismaTx.comparison.deleteMany({
			where: {
				userId: userId,
			},
		});
	}
}
