import {
	type PrismaClient,
	Order,
	User,
	Cart,
	Product,
	OrderStatus,
	UserRole,
	Prisma,
} from "@prisma/client";
import { TRPCError } from "@trpc/server";
import { injectable } from "inversify";

import { IOrderService } from "../interfaces/IOrderService";
import { prisma } from "~/server/db";
import {
	ChangeOrderStatusArgs,
	CreateOrderArgs,
	GetOrdersArgs,
} from "../validations";
import { DeliveryType } from "~/utils/constants";

@injectable()
export class OrderService implements IOrderService {
	protected prismaTx: PrismaClient;

	constructor() {
		this.prismaTx = prisma;
	}

	async getOrders(args: GetOrdersArgs & { userId: string }): Promise<{
		orders: (Order & { user: User; carts: (Cart & { product: Product })[] })[];
		count: number;
	}> {
		const { page, limit, userId } = args;

		const user = await this.prismaTx.user.findUnique({
			where: {
				id: userId,
			},
		});

		if (!user) {
			throw new TRPCError({
				code: "NOT_FOUND",
				message: "Користувача не знайдено",
			});
		}

		const isAdmin = user.role === UserRole.ADMIN;

		const where: Prisma.OrderWhereInput = {};

		if (!isAdmin) {
			where.userId = userId;
		}

		return this.prismaTx.$transaction(async (tx) => {
			const orders = await tx.order.findMany({
				where,
				include: {
					user: true,
					carts: {
						include: {
							product: true,
						},
					},
				},
				take: limit,
				skip: (page - 1) * limit,
				orderBy: {
					shortId: "desc",
				},
			});

			const count = await tx.order.count({ where });

			return { orders, count };
		});
	}

	async createOrder(args: CreateOrderArgs): Promise<Order> {
		const { userId, deliveryType, ...restArgs } = args;

		if(deliveryType === DeliveryType.PostOfficeNumberCheck) {
			if(!args.postOfficeNumber) {
				throw new TRPCError({
					code: "BAD_REQUEST",
					message: "Номер відділення не може бути порожній",
				});
			}
		}

		if(deliveryType === DeliveryType.AddressDeliveryCheck) {
			if(!args.addressDelivery) {
				throw new TRPCError({
					code: "BAD_REQUEST",
					message: "Адреса доставки не може бути порожньою",
				});
			}
		}

		const user = await this.prismaTx.user.findUnique({
			where: {
				id: userId,
			},
		});

		if (!user) {
			throw new TRPCError({
				code: "NOT_FOUND",
				message: "Користувача не знайдено",
			});
		}

		const carts = await this.prismaTx.cart.findMany({
			include: {
				product: true,
			},
			where: {
				userId,
				orderId: null,
			},
		});

		if (carts.length === 0) {
			throw new TRPCError({
				code: "BAD_REQUEST",
				message: "Кошик порожній",
			});
		}

		const cartIds = carts.map((c) => c.id);

		const totalPrice = carts.reduce(
			(acc, cart) => acc + cart.product.price * cart.quantity,
			0,
		);

		const lastOrder = await prisma.order.findFirst({
			orderBy: {
				shortId: "desc",
			},
		});

		const shortId = lastOrder ? lastOrder.shortId + 1 : 1;

		const order = await this.prismaTx.order.create({
			data: {
				shortId,
				status: OrderStatus.NEW,
				totalPrice,
				userId,
				...restArgs,
			},
		});

		await this.prismaTx.cart.updateMany({
			where: {
				id: {
					in: cartIds,
				},
			},
			data: {
				orderId: order.id,
			},
		});

		return order;
	}

	async changeOrderStatus(args: ChangeOrderStatusArgs): Promise<Order> {
		const { orderId, status } = args;

		const order = await this.prismaTx.order.findUnique({
			where: {
				id: orderId,
			},
		});

		if (!order) {
			throw new TRPCError({
				code: "NOT_FOUND",
				message: "Замовлення не знайдено",
			});
		}

		if (status === OrderStatus.NEW) {
			throw new TRPCError({
				code: "BAD_REQUEST",
				message: "Статус замовлення не може бути змінений на новий",
			});
		}

		if (status === order.status) {
			throw new TRPCError({
				code: "BAD_REQUEST",
				message: "Статус замовлення вже " + status,
			});
		}

		return this.prismaTx.order.update({
			where: {
				id: orderId,
			},
			data: {
				status,
			},
		});
	}
}
