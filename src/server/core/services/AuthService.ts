import { type PrismaClient } from "@prisma/client";
import { inject, injectable } from "inversify";

import { SignInArgs } from "../validations";
import { prisma } from "~/server/db";
import { SYMBOLS } from "~/server/constants/symbols";
import type { IAuthService } from "../interfaces/IAuthService";
import type { IUserService } from "../interfaces/IUserService";

@injectable()
export class AuthService implements IAuthService {
	protected prismaTx: PrismaClient;

	constructor(@inject(SYMBOLS.IUserService) private userService: IUserService) {
		this.prismaTx = prisma;
	}

	async signIn({ email, password }: SignInArgs) {
		const user = await this.userService.getUserByEmail(email);

		if (!user) {
			throw new Error("Неправильний пароль");
		}

		const isPasswordValid = await this.userService.verifyPassword(
			user,
			password,
		);

		if (!isPasswordValid) {
			throw new Error("Неправильний пароль");
		}

		return {
			...this.userService.normalizeUserForSession(user),
		};
	}
}
