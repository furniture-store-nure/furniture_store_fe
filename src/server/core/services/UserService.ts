import { User, type PrismaClient } from "@prisma/client";
import { TRPCError } from "@trpc/server";
import { injectable } from "inversify";
import { hash, verify } from "argon2";

import { prisma } from "~/server/db";
import { SessionUser } from "../interfaces/SessionUser";
import { IUserService } from "../interfaces/IUserService";
import {
	SignUpArgs,
	UpdateUserArgs,
	UpdateUserEmailArgs,
	UpdateUserPasswordArgs,
} from "../validations";

@injectable()
export class UserService implements IUserService {
	protected prismaTx: PrismaClient;

	constructor() {
		this.prismaTx = prisma;
	}

	normalizeUserForSession = (user: User): SessionUser => {
		return {
			id: user.id,
			email: user.email,
			role: user.role,
		};
	};

	async createUser(args: SignUpArgs): Promise<User | null> {
		const { email, password, firstName, middleName, lastName } = args;

		const isExisting = await this.isExisting(email);

		if (isExisting) {
			throw new TRPCError({
				message: "Ця електронна адреса вже зайнята",
				code: "BAD_REQUEST",
			});
		}

		const hashedPassword = await this.hashPassword(password);

		return await this.prismaTx.user.create({
			data: {
				email,
				password: hashedPassword,
				firstName,
				middleName,
				lastName,
			},
		});
	}

	async getUserById(userId: string) {
		const user = await this.prismaTx.user.findUnique({
			where: {
				id: userId,
			},
		});

		if (!user) {
			throw new TRPCError({
				code: "NOT_FOUND",
				message: "Користувач не знайдений",
			});
		}

		return user;
	}

	async updateUser(args: UpdateUserArgs & { userId: string }) {
		const { userId, firstName, middleName, lastName } = args;

		const user = await this.prismaTx.user.findUnique({
			where: {
				id: userId,
			},
		});

		if (!user) {
			throw new TRPCError({
				code: "NOT_FOUND",
				message: "Користувач не знайдений",
			});
		}

		return await this.prismaTx.user.update({
			where: {
				id: userId,
			},
			data: {
				firstName,
				middleName,
				lastName,
			},
		});
	}

	async updateUserPassword(args: UpdateUserPasswordArgs & { userId: string }) {
		const { userId, oldPassword, newPassword } = args;

		const user = await this.prismaTx.user.findUnique({
			where: {
				id: userId,
			},
		});

		if (!user) {
			throw new TRPCError({
				code: "NOT_FOUND",
				message: "Користувач не знайдений",
			});
		}

		const isPasswordValid = await this.verifyPassword(user, oldPassword);

		if (!isPasswordValid) {
			throw new Error("Неправильний пароль");
		}

		const hashedPassword = await this.hashPassword(newPassword);

		return this.prismaTx.user.update({
			data: {
				password: hashedPassword,
			},
			where: {
				id: userId,
			},
		});
	}

	async updateUserEmail(args: UpdateUserEmailArgs & { userId: string }) {
		const { userId, newEmail, password } = args;

		const user = await this.prismaTx.user.findUnique({
			where: {
				id: userId,
			},
		});

		if (!user) {
			throw new TRPCError({
				code: "NOT_FOUND",
				message: "Користувач не знайдений",
			});
		}

		const isExisting = await this.isExisting(newEmail);

		if (isExisting) {
			throw new TRPCError({
				message: "Ця електронна адреса вже зайнята",
				code: "BAD_REQUEST",
			});
		}

		const isPasswordValid = await this.verifyPassword(user, password);

		if (!isPasswordValid) {
			throw new Error("Неправильний пароль");
		}

		return this.prismaTx.user.update({
			data: {
				email: newEmail,
			},
			where: {
				id: userId,
			},
		});
	}

	async getUserByEmail(email: string) {
		const user = await this.prismaTx.user.findUnique({
			where: {
				email: email,
			},
		});

		return user;
	}

	async isExisting(email: string) {
		return (
			(await this.prismaTx.user.count({
				where: {
					email,
				},
			})) > 0
		);
	}

	async hashPassword(password: string) {
		return await hash(password);
	}

	async verifyPassword(user: User, password: string) {
		if (!user.password) {
			return false;
		}

		return verify(user.password, password);
	}
}
