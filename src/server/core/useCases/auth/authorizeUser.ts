import { SYMBOLS } from "~/server/constants/symbols";
import type { IUserService } from "../../interfaces/IUserService";
import { container } from "../../inversify.config";
import type { SignInArgs } from "../../validations";

const userService = container.get<IUserService>(SYMBOLS.IUserService);

export async function authorizeUser({ email, password }: SignInArgs) {
	const user = await userService.getUserByEmail(email);

	if (!user) {
		throw new Error("Неправильний пароль");
	}

	const isPasswordValid = await userService.verifyPassword(user, password);

	if (!isPasswordValid) {
		throw new Error("Неправильний пароль");
	}

	return userService.normalizeUserForSession(user);
}
