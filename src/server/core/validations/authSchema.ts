import { z } from "zod";

export const signInSchema = z.object({
	email: z.string().email("Ел. пошта не вірно введена"),
	password: z
		.string()
		.regex(
			new RegExp(/^(?=.*\d)(?:(?=.*[a-z])|(?=.*[A-Z])).{8,}$/),
			"Пароль повинен містити 8+ символів, принаймні одну цифру та принаймні одну літеру",
		),
});

export type SignInArgs = z.infer<typeof signInSchema>;

export const signUpSchema = z
	.object({
		email: z.string().email("Ел. пошта не вірно введена"),
		password: z
			.string()
			.regex(
				new RegExp(/^(?=.*\d)(?:(?=.*[a-z])|(?=.*[A-Z])).{8,}$/),
				"Пароль повинен містити 8+ символів, принаймні одну цифру та принаймні одну літеру",
			),
		repeatPassword: z.string(),
		firstName: z
			.string()
			.regex(
				new RegExp(/^[a-zA-Zа-щА-ЩіІїЇєЄґҐ']+$/),
				"Ім'я повинно містити лише літери (жодних символів чи цифр).",
			),
		middleName: z
			.string()
			.regex(
				new RegExp(/^[a-zA-Zа-щА-ЩіІїЇєЄґҐ']+$/),
				"Друге ім'я повинно містити лише літери (без символів та цифр).",
			),
		lastName: z
			.string()
			.regex(
				new RegExp(/^[a-zA-Zа-щА-ЩіІїЇєЄґҐ']+$/),
				"Прізвище повинно містити лише літери (без символів та цифр).",
			),
	})
	.refine((data) => data.password === data.repeatPassword, {
		message: "Паролі повинні збігатися",
	});

export type SignUpArgs = z.infer<typeof signUpSchema>;

export const updateUserSchema = z.object({
	firstName: z
		.string()
		.regex(
			new RegExp(/^[a-zA-Zа-щА-ЩіІїЇєЄґҐ']+$/),
			"Ім'я повинно містити лише літери (жодних символів чи цифр).",
		),
	middleName: z
		.string()
		.regex(
			new RegExp(/^[a-zA-Zа-щА-ЩіІїЇєЄґҐ']+$/),
			"Друге ім'я повинно містити лише літери (без символів та цифр).",
		),
	lastName: z
		.string()
		.regex(
			new RegExp(/^[a-zA-Zа-щА-ЩіІїЇєЄґҐ']+$/),
			"Прізвище повинно містити лише літери (без символів та цифр).",
		),
});

export type UpdateUserArgs = z.infer<typeof updateUserSchema>;

export const updateUserPasswordSchema = z.object({
	oldPassword: z
		.string()
		.regex(
			new RegExp(/^(?=.*\d)(?:(?=.*[a-z])|(?=.*[A-Z])).{8,}$/),
			"Пароль повинен містити 8+ символів, принаймні одну цифру та принаймні одну літеру",
		),
	newPassword: z
		.string()
		.regex(
			new RegExp(/^(?=.*\d)(?:(?=.*[a-z])|(?=.*[A-Z])).{8,}$/),
			"Пароль повинен містити 8+ символів, принаймні одну цифру та принаймні одну літеру",
		),
});

export type UpdateUserPasswordArgs = z.infer<typeof updateUserPasswordSchema>;

export const updateUserEmailSchema = z.object({
	newEmail: z.string().email("Ел. пошта не вірно введена"),
	password: z
		.string()
		.regex(
			new RegExp(/^(?=.*\d)(?:(?=.*[a-z])|(?=.*[A-Z])).{8,}$/),
			"Пароль повинен містити 8+ символів, принаймні одну цифру та принаймні одну літеру",
		),
});

export type UpdateUserEmailArgs = z.infer<typeof updateUserEmailSchema>;
