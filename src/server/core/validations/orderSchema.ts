import { OrderStatus } from "@prisma/client";
import { z } from "zod";

export const getOrdersSchema = z.object({
	page: z.number().min(1),
	limit: z.number().min(1),
});

export type GetOrdersArgs = z.infer<typeof getOrdersSchema>;

export const changeOrderStatusSchema = z.object({
	orderId: z.string(),
	status: z.nativeEnum(OrderStatus),
});

export type ChangeOrderStatusArgs = z.infer<typeof changeOrderStatusSchema>;
