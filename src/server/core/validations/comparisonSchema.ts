import { z } from "zod";

export const createComparisonSchema = z.object({
	productId: z.string(),
	userId: z.string(),
});

export type CreateComparisonArgs = z.infer<typeof createComparisonSchema>;

export const getComparisonsSchema = z.object({
	userId: z.string(),
});

export type GetComparisonsArgs = z.infer<typeof getComparisonsSchema>;

export const deleteComparisonSchema = z.object({
	userId: z.string(),
});

export type DeleteComparisonArgs = z.infer<typeof deleteComparisonSchema>;