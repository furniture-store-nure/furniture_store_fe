import { Material, POSTS, Security, TypeProduct } from "@prisma/client";
import { z } from "zod";
import { DeliveryType } from "~/utils/constants";

export const getProductsSchema = z.object({
	page: z.number().min(1),
	limit: z.number().min(1),
});

export type GetProductsArgs = z.infer<typeof getProductsSchema>;

export const priceFiltersSchema = z.object({
	priceFrom: z.number().optional(),
	priceTo: z.number().optional(),
});

export const getProductsScrollSchema = z.object({
	limit: z.number().min(2).max(100).default(2),
	cursor: z.string().optional(),
	manufacturerId: z.string().optional(),
	type: z.nativeEnum(TypeProduct).optional(),
	material: z.nativeEnum(Material).optional(),
	security: z.nativeEnum(Security).optional(),
	priceFilters: priceFiltersSchema.array().optional(),
});

export type GetProductsScrollArgs = z.infer<typeof getProductsScrollSchema>;

export const getProductByIdSchema = z.object({
	productId: z.string(),
});

export type GetProductByIdArgs = z.infer<typeof getProductByIdSchema>;

export const productSchema = z.object({
	name: z.string(),
	type: z.nativeEnum(TypeProduct),
	material: z.nativeEnum(Material),
	security: z.nativeEnum(Security),
	manufacturerId: z.string(),
	width: z.string(),
	height: z.string(),
	weight: z.string(),
	price: z.number(),
	logo: z.string(),
});

export type ProductArgs = z.infer<typeof productSchema>;

export const editProductSchema = productSchema.extend({
	id: z.string(),
});

export type EditProductArgs = z.infer<typeof editProductSchema>;

export const deleteProductSchema = z.object({
	productId: z.string(),
});

export type DeleteProductArgs = z.infer<typeof deleteProductSchema>;

export const createOrderSchema = z.object({
	userId: z.string(),
	postName: z.nativeEnum(POSTS),
	deliveryType: z.nativeEnum(DeliveryType),
	postOfficeNumber: z.string().optional(),
	addressDelivery: z.string().optional(),
});

export type CreateOrderArgs = z.infer<typeof createOrderSchema>;
