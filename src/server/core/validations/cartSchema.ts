import { z } from "zod";

export const getCartSchema = z.object({
	userId: z.string(),
});

export type GetCartArgs = z.infer<typeof getCartSchema>;

export const createCartSchema = z.object({
	productId: z.string(),
	userId: z.string(),
});

export type CreateCartArgs = z.infer<typeof createCartSchema>;

export const updateCartSchema = z.object({
	cartId: z.string(),
	quantity: z.number(),
	orderId: z.string().optional(),
});

export type UpdateCartArgs = z.infer<typeof updateCartSchema>;
