import {
	type Cart,
	Material,
	OrderStatus,
	POSTS,
	PrismaClient,
	TypeProduct,
	UserRole,
	Security,
} from "@prisma/client";

import { bedLogo, chairLogo, logoSofa } from "./logos";
import { DeliveryType } from "~/utils/constants";

const prisma = new PrismaClient();

//Zaq1xsw2cde#
const password =
	"$argon2id$v=19$m=65536,t=3,p=4$Hca1mFVPh/DE0u9fUuBFKw$fDmLaNrgbj5gDXMo9VsvpEhpkuZFeJn4kxHY4q4SiwI";

const posts = [POSTS.NovaPoshta, POSTS.UkrPoshta];

const typesProduct = [TypeProduct.BED, TypeProduct.CHAIR, TypeProduct.SOFA];

const materials = [Material.LEATHER, Material.TEXTILE];

const securities = [
	Security.SAFE_FOR_CHILDREN,
	Security.CONTAINS_SMALL_DETAILS,
];

const orderStatuses = [
	OrderStatus.NEW,
	OrderStatus.REJECTED,
	OrderStatus.FINISHED,
];

const deliveryTypes = [
	DeliveryType.AddressDeliveryCheck,
	DeliveryType.PostOfficeNumberCheck,
];

const productNames = [
	"Classic Oak",
	"Modern Pine",
	"Vintage Maple",
	"Rustic Birch",
	"Contemporary Walnut",
	"Minimalist Ash",
	"Industrial Cedar",
	"Traditional Mahogany",
	"Art Deco Ebony",
	"Scandinavian Spruce",
	"Baroque Cherry",
	"Rococo Rosewood",
	"Victorian Teak",
	"Edwardian Sycamore",
	"Colonial Fir Coffee",
	"Shaker Lime",
	"Gothic Poplar",
	"Neoclassical Beech",
	"Renaissance Alder",
	"Byzantine Hickory",
	"Art Nouveau Hornbeam",
	"Craftsman Juniper",
	"Mission Style Redwood",
	"French Provincial Yew Pedestal",
	"Mid-Century Modern Dogwood",
	"Bohemian Bamboo",
]

const firstNames = [
	"Alex",
	"John",
	"Jane",
	"Doe",
	"Jim",
	"Jill",
	"Joe",
	"Jill",
	"Jim",
];
const lastNames = [
	"Shapar",
	"Smith",
	"Johnson",
	"Brown",
	"Doe",
	"Jones",
	"Jones",
	"Jones",
	"Jones",
];
const middleNames = [
	"Alexeevich",
	"Ivanovich",
	"Petrovich",
	"Sidorovich",
	"Vasilievich",
	"Sergeevich",
	"Sergeevich",
	"Sergeevich",
	"Sergeevich",
];

async function createUser(
	role: UserRole,
	firstName: string,
	lastName: string,
	middleName: string,
	email: string,
	password: string,
) {
	return await prisma.user.create({
		data: {
			role,
			firstName,
			lastName,
			middleName,
			email,
			password,
		},
	});
}

async function createManufacturer(name: string) {
	return await prisma.manufacturer.create({
		data: {
			name,
		},
	});
}

async function createProduct(
	name: string,
	type: TypeProduct,
	material: Material,
	security: Security,
	height: string,
	width: string,
	weight: string,
	price: number,
	manufacturerId: string,
) {
	let logo;

	if (type === TypeProduct.SOFA) {
		logo = logoSofa;
	} else if (type === TypeProduct.CHAIR) {
		logo = chairLogo;
	} else {
		logo = bedLogo;
	}

	return await prisma.product.create({
		data: {
			name,
			logo,
			type,
			material,
			security,
			height,
			width,
			weight,
			price,
			manufacturerId,
		},
	});
}

async function createCart(productId: string, userId: string, quantity: number) {
	return await prisma.cart.create({
		data: {
			productId,
			userId,
			quantity,
		},
	});
}

async function createOrder(
	userId: string,
	postName: POSTS,
	totalPrice: number,
	status: OrderStatus,
	postOfficeNumber?: string,
	addressDelivery?: string,
) {
	const lastOrder = await prisma.order.findFirst({
		orderBy: {
			shortId: "desc",
		},
	});

	const shortId = lastOrder ? lastOrder.shortId + 1 : 1;

	return await prisma.order.create({
		data: {
			userId,
			shortId,
			postName,
			postOfficeNumber,
			addressDelivery,
			totalPrice,
			status,
		},
	});
}

async function cleanupDatabase() {
	await prisma.account.deleteMany({});
	await prisma.cart.deleteMany({});
	await prisma.manufacturer.deleteMany({});
	await prisma.order.deleteMany({});
	await prisma.product.deleteMany({});
	await prisma.user.deleteMany({});
}

async function main() {
	await cleanupDatabase();

	const users = [];
	const manufacturers = [];
	const products = [];
	let carts: (Cart & { price: number })[] = [];

	await createUser(
		UserRole.ADMIN,
		"Alex",
		"Shapar",
		"Alexeevich",
		"sshapar13371@gmail.com",
		password,
	);

	//create users
	const randomAmountOfUsers = 50;
	for (let i = 0; i < randomAmountOfUsers; i++) {
		const firstName = firstNames[i % firstNames.length]!;
		const lastName = lastNames[i % lastNames.length]!;
		const middleName = middleNames[i % middleNames.length]!;

		const user = await createUser(
			UserRole.USER,
			firstName,
			lastName,
			middleName,
			`email${i}@gmail.com`,
			password,
		);

		users.push(user);
	}

	//create manufacturers
	const randomAmountOfManufacturers = 50;
	for (let i = 0; i < randomAmountOfManufacturers; i++) {
		const manufacturer = await createManufacturer(`manufacturer${i}`);

		manufacturers.push(manufacturer);
	}

	//create products
	const randomAmountOfProducts = 50;
	for (let i = 0; i < randomAmountOfProducts; i++) {
		const manufacturer = manufacturers[i % manufacturers.length]!;
		const type = typesProduct[i % typesProduct.length]!;
		const security = securities[i % securities.length]!;
		const material = materials[i % materials.length]!;
		const name = productNames[i % productNames.length]!;

		const product = await createProduct(
			name,
			type,
			material,
			security,
			"100",
			"100",
			"100",
			i === 0 ? 1000 : i * 1000,
			manufacturer.id,
		);

		products.push(product);
	}

	//create carts
	const randomAmountOfCarts = 50;
	for (let i = 0; i < randomAmountOfCarts; i++) {
		const product = products[i % products.length]!;
		const user = users[i % users.length]!;

		const cart = await createCart(product.id, user.id, (i % 10) + 1);

		carts.push({ ...cart, price: product.price });
	}

	//create orders
	const randomAmountOfOrders = 50;
	for (let i = 0; i < randomAmountOfOrders; i++) {
		const user = users[i % users.length]!;
		const cart = carts[i % carts.length]!;
		const deliveryType = deliveryTypes[i % deliveryTypes.length]!;
		let addressDelivery;
		let postOfficeNumber;

		if (deliveryType === DeliveryType.AddressDeliveryCheck) {
			addressDelivery = "pr. Gagarina 102";
		} else {
			postOfficeNumber = "100";
		}

		const order = await createOrder(
			user.id,
			posts[i % posts.length]!,
			cart.price * cart.quantity,
			orderStatuses[i % orderStatuses.length]!,
			postOfficeNumber,
			addressDelivery,
		);

		await prisma.cart.update({
			where: {
				id: cart.id,
			},
			data: {
				orderId: order.id,
			},
		});

		carts = carts.filter((c) => c.id !== cart.id);
	}
}

main()
	.then(async () => {
		console.log("Seeding finished.");
		await prisma.$disconnect();
	})
	.catch(async (e) => {
		console.error(e);
		await prisma.$disconnect();
		process.exit(1);
	});
