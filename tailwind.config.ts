import type { Config } from "tailwindcss";

export default {
	content: ["./src/**/*.{js,ts,jsx,tsx}"],
	important: "#__next",
	future: {
		hoverOnlyWhenSupported: true,
	},
	theme: {
		fontFamily: {
			poppins: ["Poppins", "sans-serif"],
			montserrat: ["Montserrat", "sans-serif"],
			openSans: ["Open Sans", "sans-serif"],
		},
		extend: {
			colors: {
				bgLight: "#F5F1ED",

				beige: "#E0DDDA",
				turquoise: "#5FC8C3",
				turquoiseDark: "#0E9AA1",
				gray: "#454545",
				grayDark: "#323640",
				grayMedium2: "#808080",
				grayMedium: "#BEBEBE",
				grayLight: "#FAF9F7",
				red: "#E5325C",
				green: "#27AE60",
				greenLight: "#EEFBEF"
			},
			boxShadow: {
				card: "0px 4px 12px 0px rgba(179, 182, 186, 0.50)",
			},
		},
	},
	plugins: [],
} satisfies Config;
