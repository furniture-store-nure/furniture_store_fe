# Furniture Store

## Технології

- Next.js 13
- React 18
- Prisma
- NextAuth
- TRPC
- TailwindCSS
- Redux
- Inversify
- TypeScript
- Zod

## Початок роботи
### Крок 1: Встановіть залежності

Спочатку встановіть залежності проекту:

```bash
pnpm i
```

### Крок 2: Налаштування змінних середовища

Створіть файл .env в кореневому каталозі та додайте наступні змінні середовища:

```bash
DATABASE_URL=
NEXTAUTH_SECRET=
NEXTAUTH_URL="http://localhost:3000/"
NODE_ENV=development
PORT=3000
```

### Крок 3: Застосування миграцій бази даних

Застосуйте миграції бази даних:

```bash
npx prisma migrate deploy
```

### Крок 4: Генерація Prisma Client

```bash
npx prisma generate
```

### Крок 5: Запустіть сервер розробки

Запустіть сервер розробки:

```bash 
pnpm dev
```

Відкрити [http://localhost:3000](http://localhost:3000) у вашому браузері, щоб побачити результат.